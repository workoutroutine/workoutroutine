### Task description

(Describe the task to be performed.)
(Tasks can be a constant-feature refactor, upgrading libraries, adding a test suite, improving the CI...)

### Future benefits

(Suggest the benefits provided by fulfilling this task.)
(Benefits should be understandable by non-technical collaborators, e.g. "will improve average response time", "will enable better error reporting", "necessary for building <other feature>"...)

----

### To-Do list

( :warning: This section is reserved to the developers :warning: )
(List technical tasks as the To-Do list)

/label ~"Type::Tech task" ~"Status::To Do" 