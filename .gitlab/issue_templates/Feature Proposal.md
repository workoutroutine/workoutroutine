### Problem to solve

As a X, I want to Y..
(Include use cases, benefits, detailed description...)

### Proposal

(Describe how to respond to the need, including screenshots/sketches when possible)

--- 

### Technical Implementation

( :warning: This section is reserved to the developers :warning: )
(List technical tasks as the To-Do list)

/label ~"Type::Feature" ~"Status::To Do" 
