#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

from setuptools import find_packages, setup

setup(
    name='workoutroutine-api',
    use_scm_version={
        "root": "..",
        "relative_to": __file__,
        'write_to': 'api/wrapi/version.py',
        'write_to_template': '__version__ = "{version}"\n',
        'tag_regex': r'^(?P<prefix>v)?(?P<version>[^\+]+)(?P<suffix>.*)?$'
    },
    setup_requires=['setuptools_scm'],
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'auth0-python',
        'flask',
        'Flask-CORS',
        'Flask-Mail',
        'flask-sqlalchemy',
        'flask-migrate',
        'elasticsearch',
        'elasticsearch_dsl',
        'gunicorn',
        'sentry-sdk[flask]',
        'python-dotenv',
        'python-jose',
        'pymemcache',
        'setuptools_scm',
        'six'
    ],
)
