"""Theme table initialization

Revision ID: e24e1d8a735d
Revises: 19fda808a45c
Create Date: 2020-12-15 13:57:20.468746

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e24e1d8a735d'
down_revision = '19fda808a45c'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    theme_table = op.create_table('theme',
                                  sa.Column('id', sa.Integer(),
                                            nullable=False),
                                  sa.Column('slug', sa.String(
                                      length=64), nullable=True),
                                  sa.Column('name', sa.Text(), nullable=True),
                                  sa.PrimaryKeyConstraint('id')
                                  )
    op.create_index(op.f('ix_theme_slug'), 'theme', ['slug'], unique=True)
    op.bulk_insert(theme_table, [{
        "slug": "fitness",
        "name": "{\"en\": \"Fitness\", \"fr\": \"Fitness\"}"
    }, {
        "slug": "silat",
        "name": "{\"en\": \"Pencak Silat\", \"fr\": \"Pencak Silat\"}"
    }, {
        "slug": "boxefr",
        "name": "{\"en\": \"French boxing\", \"fr\": \"Boxe française\"}"
    }
    ])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_theme_slug'), table_name='theme')
    op.drop_table('theme')
    # ### end Alembic commands ###
