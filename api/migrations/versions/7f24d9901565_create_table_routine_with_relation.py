"""Create table routine with relation

Revision ID: 7f24d9901565
Revises: 5b596b1f367e
Create Date: 2020-12-29 14:38:44.918119

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7f24d9901565'
down_revision = '5b596b1f367e'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('routine',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('uuid', sa.String(length=36), nullable=True),
                    sa.Column('user_id', sa.Integer(), nullable=True),
                    sa.Column('ts_created', sa.DateTime(), nullable=True),
                    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
                    sa.PrimaryKeyConstraint('id')
                    )
    op.create_index(op.f('ix_routine_uuid'), 'routine', ['uuid'], unique=False)
    op.create_table('routine_exercices',
                    sa.Column('routine_id', sa.Integer(), nullable=False),
                    sa.Column('exercice_id', sa.Integer(), nullable=False),
                    sa.Column('position', sa.Integer(), nullable=False),
                    sa.ForeignKeyConstraint(
                        ['exercice_id'], ['exercice.id'], ),
                    sa.ForeignKeyConstraint(['routine_id'], ['routine.id'], ),
                    sa.PrimaryKeyConstraint(
                        'routine_id', 'exercice_id', 'position')
                    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('routine_exercices')
    op.drop_index(op.f('ix_routine_uuid'), table_name='routine')
    op.drop_table('routine')
    # ### end Alembic commands ###
