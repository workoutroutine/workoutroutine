"""adds updated timestamp to exercice table

Revision ID: 8419dd83019b
Revises: 0344aa664677
Create Date: 2021-01-01 03:19:10.722492

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8419dd83019b'
down_revision = '0344aa664677'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('exercice', sa.Column(
        'ts_updated', sa.DateTime(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('exercice', 'ts_updated')
    # ### end Alembic commands ###
