#!/bin/sh

readonly PROGDIR=$(dirname "$0")

cd "${PROGDIR}" || exit;
flask db upgrade;
flask stat init;
exec gunicorn wrapi:app --workers 4 --bind 0.0.0.0:5000 --timeout 1200;
