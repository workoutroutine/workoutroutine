#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

from wrapi.exercise.model import Exercise, ExerciseChange
from wrapi.routine.model import Routine
from wrapi.user.model import User

TEST_EXERCICE = None
TEST_USER = User(
    id=1,
    name='FooBar',
    email='foo@bar.com',
    email_verified=True,
    icon='icon',
    auth_id='auth_id',
    group_id=1
)
TEST_EXERCICE_CHANGE = None
TEST_ROUTINE = None


def load_exported_var_from_db(db):
    global TEST_EXERCICE
    global TEST_EXERCICE_CHANGE
    TEST_EXERCICE = Exercise().get_by_id(1)
    TEST_EXERCICE_CHANGE = ExerciseChange(
        parent_id=TEST_EXERCICE.id,
        author_id=1,
        name=TEST_EXERCICE.name,
        description=TEST_EXERCICE.description,
        midtime=TEST_EXERCICE.midtime,
        family_id=TEST_EXERCICE.family_id,
        theme_id=TEST_EXERCICE.theme_id,
        ts_created=TEST_EXERCICE.ts_created
    )


def load_exercise_fixtures(db):
    db.session.add(TEST_EXERCICE_CHANGE)
    db.session.commit()


def load_user_fixtures(db):
    global TEST_USER
    db.session.add(TEST_USER)
    db.session.commit()
    TEST_USER = User().get_by_auth_id('auth_id')


def load_routine_fixtures(db):
    global TEST_ROUTINE
    exercises = [
        Exercise().get_by_id(1),
        Exercise().get_by_id(2),
        Exercise().get_by_id(3)
    ]
    routine = Routine(
        size=len(exercises),
        family=[x.family.slug for x in exercises],
        theme=[x.theme.slug for x in exercises]
    )
    routine.exercises = exercises
    routine.user_id = TEST_USER.id
    routine.save()
    TEST_ROUTINE = Routine.query.get(1)
