#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

import uuid

import pytest

from wrapi.utils import base64Decode, base64Encode


def test_base64Encode():
    cases = [
        "00000000-0000-0000-0000-000000000000",
        "e8ebd5a4-236c-47e8-b25c-a30a8d868438",
        "c82166a5-3b01-4cd6-8ec4-c76a5b37d398",
        "f44d6bc6-99a2-47a0-a8b1-0a82f37274e2",
        "5bd8f4ea-3699-4104-8bce-6e3a2847009d",
        "bcb8dbec-b743-408a-8ebd-756cf0ca8d91",
        "cef35d89-a812-46c0-b856-14742267f389",
        "c725e1da-fbff-4d30-a9db-60ff4ed8a007",
        "fbd6779f-70f3-46e9-873c-4561f7614ab3",
        "b5f41221-4ad3-40c8-932d-59e3de1afd8b",
        "b2c95081-82db-4dc9-82b3-bb08e05e0d26"
    ]
    expected = [
        "AAAAAAAAAAAAAAAAAAAAAA",
        "6OvVpCNsR-iyXKMKjYaEOA",
        "yCFmpTsBTNaOxMdqWzfTmA",
        "9E1rxpmiR6CosQqC83J04g",
        "W9j06jaZQQSLzm46KEcAnQ",
        "vLjb7LdDQIqOvXVs8MqNkQ",
        "zvNdiagSRsC4VhR0ImfziQ",
        "xyXh2vv_TTCp22D_TtigBw",
        "-9Z3n3DzRumHPEVh92FKsw",
        "tfQSIUrTQMiTLVnj3hr9iw",
        "sslQgYLbTcmCs7sI4F4NJg"
    ]
    for index, case in enumerate(cases):
        res = base64Encode(u=uuid.UUID(case))
        assert res == expected[index]


def test_base64EncodeTypeError():
    cases = [
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        "00000000-0000-0000-0000-000000000000",
        42
    ]
    for case in cases:
        with pytest.raises(TypeError):
            assert base64Encode(u=case) is None


def test_base64Decode():
    cases = [
        "AAAAAAAAAAAAAAAAAAAAAA",
        "6OvVpCNsR-iyXKMKjYaEOA",
        "yCFmpTsBTNaOxMdqWzfTmA",
        "9E1rxpmiR6CosQqC83J04g",
        "W9j06jaZQQSLzm46KEcAnQ",
        "vLjb7LdDQIqOvXVs8MqNkQ",
        "zvNdiagSRsC4VhR0ImfziQ",
        "xyXh2vv_TTCp22D_TtigBw",
        "-9Z3n3DzRumHPEVh92FKsw",
        "tfQSIUrTQMiTLVnj3hr9iw",
        "sslQgYLbTcmCs7sI4F4NJg"
    ]
    expected = [
        "00000000-0000-0000-0000-000000000000",
        "e8ebd5a4-236c-47e8-b25c-a30a8d868438",
        "c82166a5-3b01-4cd6-8ec4-c76a5b37d398",
        "f44d6bc6-99a2-47a0-a8b1-0a82f37274e2",
        "5bd8f4ea-3699-4104-8bce-6e3a2847009d",
        "bcb8dbec-b743-408a-8ebd-756cf0ca8d91",
        "cef35d89-a812-46c0-b856-14742267f389",
        "c725e1da-fbff-4d30-a9db-60ff4ed8a007",
        "fbd6779f-70f3-46e9-873c-4561f7614ab3",
        "b5f41221-4ad3-40c8-932d-59e3de1afd8b",
        "b2c95081-82db-4dc9-82b3-bb08e05e0d26"
    ]
    for index, case in enumerate(cases):
        res = base64Decode(u=case)
        assert str(res) == expected[index]


def test_base64DecodeTypeError():
    cases = [
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        uuid.UUID("00000000-0000-0000-0000-000000000000"),
        42
    ]
    for case in cases:
        with pytest.raises(TypeError):
            assert base64Decode(u=case) is None

    assert base64Decode(u="this is not a UUID. At all.") is None
