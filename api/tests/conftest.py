#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

import pytest
import os
from alembic.command import upgrade
from alembic.config import Config
from wrapi import create_app
from wrapi import db as _db
from conf_fixtures import load_exported_var_from_db, load_exercise_fixtures, \
    load_user_fixtures, load_routine_fixtures


ALEMBIC_CONFIG = os.path.join(
    os.path.dirname(__file__),
    '../migrations/alembic.ini')

TESTDB_PATH = '/tmp/workoutroutine.test.sqlite'

_mimetype = 'application/json'
HEADERS = {
    'Content-Type': _mimetype,
    'Accept': _mimetype
}


def apply_migrations():
    """Applies all alembic migrations."""
    config = Config(ALEMBIC_CONFIG)
    upgrade(config, 'head')


@pytest.fixture(scope='session')
def app(request):
    app = create_app({
        'TESTING': True,
        'SECRET_KEY': 'dev',
        'ELASTICSEARCH_HOST': os.environ.get(
            'ELASTICSEARCH_HOST', default='localhost:9200'),
        'MEMCACHED_HOST': os.environ.get(
            'MEMCACHED_HOST', default='localhost'),
        'MEMCACHED_PORT': 11211,
        'SQLALCHEMY_DATABASE_URI': 'sqlite:///{}'.format(TESTDB_PATH),
        'SQLALCHEMY_TRACK_MODIFICATIONS': False
    })

    # Establish an application context before running the tests.
    ctx = app.app_context()
    ctx.push()

    def teardown():
        ctx.pop()

    request.addfinalizer(teardown)
    return app


@pytest.fixture(scope='session')
def db(app, request):
    if os.path.exists(TESTDB_PATH):
        os.unlink(TESTDB_PATH)

    def teardown():
        _db.drop_all()
        os.unlink(TESTDB_PATH)

    _db.app = app
    with app.app_context():
        apply_migrations()

        load_exported_var_from_db(_db)
        load_user_fixtures(_db)
        load_exercise_fixtures(_db)
        load_routine_fixtures(_db)

    request.addfinalizer(teardown)
    return _db


@pytest.fixture(scope='session')
def client(app):
    return app.test_client()


@pytest.fixture(scope='session')
def runner(app):
    return app.test_cli_runner()


@pytest.fixture(scope='function')
def session(db, request):
    """Creates a new database session for a test."""
    connection = db.engine.connect()
    transaction = connection.begin()

    options = dict(bind=connection, binds={})
    session = db.create_scoped_session(options=options)

    db.session = session

    def teardown():
        transaction.rollback()
        connection.close()
        session.remove()

    request.addfinalizer(teardown)
    return session
