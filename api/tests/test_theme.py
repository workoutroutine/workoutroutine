#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

from wrapi.theme.model import Theme
from datetime import datetime
import json


def test_model_repr(db):
    res = Theme().get_by_id(1)

    assert '{}'.format(res).startswith('<Theme ')


def test_model_to_dict(db):
    res = Theme().get_by_id(1).to_dict()

    assert isinstance(res, dict)
    assert 'slug' in res
    assert 'name' in res
    assert 'id' not in res


def test_model_name_json(db):
    theme = Theme()
    assert theme.name_json == {}
    theme.name = 'invalid json'
    assert theme.name_json == {}
    theme.name = '{"valid": "json"}'
    assert theme.name_json == {'valid': 'json'}


def test_model_get_by_id(db):
    theme = Theme().get_by_id(1)

    assert isinstance(theme, Theme)
    assert isinstance(theme.slug, str)
    assert isinstance(theme.name, str)
    assert isinstance(theme.ts_created, datetime)


def test_model_get_by_slug(db):
    ref = Theme().get_by_id(1)

    res = Theme().get_by_slug(ref.slug)
    assert res == ref

    res = Theme().get_by_slug('')
    assert res is None


def test_model_filter_by_slug(db):
    ref = Theme().get_by_id(1)

    res = Theme().filter_by_slug([ref.slug])
    assert res == [ref]

    res = Theme().filter_by_slug([])
    assert res == []


def test_model_get_all(db):
    res = Theme().get_all()

    assert len(res) > 0
    assert all([isinstance(x, Theme) for x in res])


def test_view_index(client):
    ref = Theme().get_all()
    res = client.get('/v1/theme/')
    assert res.status_code == 200
    data_json = json.loads(res.data)
    assert len(data_json) >= 0
    assert len(data_json) == len(ref)
