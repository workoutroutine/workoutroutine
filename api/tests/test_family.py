#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

from wrapi.family.model import Family
from datetime import datetime
import json


def test_model_repr(db):
    res = Family().get_by_id(1)

    assert '{}'.format(res).startswith('<Family ')


def test_model_to_dict(db):
    res = Family().get_by_id(1).to_dict()

    assert isinstance(res, dict)
    assert 'slug' in res
    assert 'name' in res
    assert 'logo' in res
    assert 'id' not in res


def test_model_name_json(db):
    family = Family()
    assert family.name_json == {}
    family.name = 'invalid json'
    assert family.name_json == {}
    family.name = '{"valid": "json"}'
    assert family.name_json == {'valid': 'json'}


def test_model_get_by_id(db):
    family = Family().get_by_id(1)

    assert isinstance(family, Family)
    assert isinstance(family.slug, str)
    assert isinstance(family.name, str)
    assert isinstance(family.logo, str)
    assert isinstance(family.ts_created, datetime)


def test_model_get_by_slug(db):
    ref = Family().get_by_id(1)

    res = Family().get_by_slug(ref.slug)
    assert res == ref

    res = Family().get_by_slug('')
    assert res is None


def test_model_filter_by_slug(db):
    ref = Family().get_by_id(1)

    res = Family().filter_by_slug([ref.slug])
    assert res == [ref]

    res = Family().filter_by_slug([])
    assert res == []


def test_model_get_all(db):
    res = Family().get_all()

    assert len(res) > 0
    assert all([isinstance(x, Family) for x in res])


def test_view_index(client):
    ref = Family().get_all()
    res = client.get('/v1/family/')
    assert res.status_code == 200
    data_json = json.loads(res.data)
    assert len(data_json) >= 0
    assert len(data_json) == len(ref)
