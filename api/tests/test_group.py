#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

from wrapi.group.model import Group
from datetime import datetime


def test_model_repr(db):
    res = Group().get_by_id(1)

    assert '{}'.format(res).startswith('<Group ')


def test_model_to_dict(db):
    res = Group().get_by_id(1).to_dict()

    assert isinstance(res, dict)
    assert 'slug' in res
    assert 'id' not in res


def test_model_get_by_id(db):
    group = Group().get_by_id(1)

    assert isinstance(group, Group)
    assert isinstance(group.slug, str)
    assert isinstance(group.ts_created, datetime)


def test_model_get_by_slug(db):
    ref = Group().get_by_id(1)

    res = Group().get_by_slug(ref.slug)
    assert res == ref

    res = Group().get_by_slug('')
    assert res is None
