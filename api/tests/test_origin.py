#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>
from wrapi.version import __version__


def test_view_version(client):
    res = client.get('/v1/version')
    assert res.status_code == 200
    assert res.data.decode("utf-8") == str(__version__)
