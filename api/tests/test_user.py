#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

from datetime import datetime
from wrapi.user.model import User
from tests.conf_fixtures import TEST_USER


def test_model_init(db):
    res = User()
    assert isinstance(res, User)
    assert res.id is None
    assert len(res.uuid) > 0


def test_model_repr(db):
    res = User()
    assert '{}'.format(res).startswith('<User ')


def test_model_to_dict(db):
    res = User.query.get(1)
    assert res is not None
    assert res.to_dict() is not None
    assert isinstance(res.to_dict(), dict)
    assert res.to_dict().get('uuid') is not None
    assert res.to_dict().get('auth_id') is not None

    res.ts_updated = None
    assert res.to_dict() is not None
    assert res.to_dict().get('ts_updated') is None
    db.session.rollback()

    res.ts_updated = datetime.utcnow()
    assert res.to_dict() is not None
    assert res.to_dict().get('ts_updated') is not None
    db.session.rollback()


def test_model_to_public_dict(db):
    res = User.query.get(1)
    assert res is not None
    assert res.to_public_dict() is not None
    assert isinstance(res.to_public_dict(), dict)
    assert res.to_public_dict().get('uuid') is not None
    assert res.to_public_dict().get('auth_id') is None


def test_model_get_by_auth_id(db):
    res = User().get_by_auth_id(auth_id=TEST_USER.auth_id)
    assert res is not None
    assert res.auth_id == TEST_USER.auth_id
    assert res.email == TEST_USER.email


def test_model_get_by_uuid(db):
    u = User.query.get(1)
    res = User().get_by_uuid(safe_id=u.safe_id)
    assert res is not None
    assert res.auth_id == u.auth_id
    assert res.email == u.email


def test_model_get_all(db):
    res = User().get_all()
    assert res is not None
    assert isinstance(res, list)
    assert len(res) > 0
