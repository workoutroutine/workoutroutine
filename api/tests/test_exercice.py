#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

import pytest
from wrapi.exercise.model import Exercise
from wrapi.family.model import Family
from wrapi.theme.model import Theme
from datetime import datetime


def test_model_repr(db):
    res = Exercise().get_by_id(1)

    assert '{}'.format(res).startswith('<Exercise ')


def test_model_to_dict(db):
    res = Exercise().get_by_id(1).to_dict()

    assert isinstance(res, dict)
    assert 'id' in res
    assert 'name' in res
    assert 'description' in res
    assert 'midtime' in res
    assert 'family' in res
    assert 'theme' in res
    assert 'uuid' not in res


def test_model_name_readable(db):
    exercise = Exercise()
    assert exercise.name_readable == ''
    exercise.name = 'invalid json'
    assert exercise.name_readable == ''
    exercise.name = '{"valid": "json"}'
    assert exercise.name_readable == ''
    exercise.name = '{"en": "exercise_name", "fr": "exercise_nom"}'
    assert exercise.name_readable == 'exercise_name'


def test_model_name_json(db):
    exercise = Exercise()
    assert exercise.name_json == {}
    exercise.name = 'invalid json'
    assert exercise.name_json == {}
    exercise.name = '{"valid": "json"}'
    assert exercise.name_json == {'valid': 'json'}


def test_model_description_json(db):
    exercise = Exercise()
    assert exercise.description_json == {}
    exercise.description = 'invalid json'
    assert exercise.description_json == {}
    exercise.description = '{"valid": "json"}'
    assert exercise.description_json == {'valid': 'json'}


def test_model_get_by_id(db):
    exercise = Exercise().get_by_id(1)

    # testing fields
    assert isinstance(exercise, Exercise)
    assert isinstance(exercise.id, int)
    assert isinstance(exercise.name, str)
    assert isinstance(exercise.description, str)
    assert isinstance(exercise.midtime, bool)
    assert isinstance(exercise.family_id, int)
    assert isinstance(exercise.theme_id, int)
    assert isinstance(exercise.ts_created, datetime)
    # testing backref
    assert isinstance(exercise.family, Family)
    assert isinstance(exercise.theme, Theme)


def test_model_get_all(db):
    res = Exercise().get_all()

    assert len(res) > 0
    assert all([isinstance(x, Exercise) for x in res])


def test_model_update(db):
    exercise = Exercise().get_by_id(1)
    other_exercise = Exercise().get_by_id(2)
    exercise.update(
        name=other_exercise.name,
        description=other_exercise.description,
        midtime=other_exercise.midtime,
        family_id=other_exercise.family_id,
        theme_id=other_exercise.theme_id
    )

    assert exercise.name == other_exercise.name
    assert exercise.description == other_exercise.description
    assert exercise.midtime == other_exercise.midtime
    assert exercise.family_id == other_exercise.family_id
    assert exercise.theme_id == other_exercise.theme_id

    with pytest.raises(TypeError):
        exercise.update()


def test_model_save(db):
    exercise = Exercise().get_by_id(1)
    other_exercise = Exercise().get_by_id(2)

    exercise.name = other_exercise.name
    exercise.description = other_exercise.description
    exercise.midtime = other_exercise.midtime
    exercise.family_id = other_exercise.family_id
    exercise.theme_id = other_exercise.theme_id
    exercise.save()

    res = Exercise().get_by_id(1)
    assert res.name == other_exercise.name
    assert res.description == other_exercise.description
    assert res.midtime == other_exercise.midtime
    assert res.family_id == other_exercise.family_id
    assert res.theme_id == other_exercise.theme_id


def test_model_getRandomEquivalent(db):
    ref = Exercise().get_by_id(1)
    res = ref.getRandomEquivalent()

    assert res.family_id == ref.family_id
    assert res.theme_id == ref.theme_id
