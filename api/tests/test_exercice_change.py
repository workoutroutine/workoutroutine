#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

from wrapi.exercise.model import ExerciseChange, Exercise
from datetime import datetime


def test_model_repr(db):
    res = ExerciseChange()

    assert '{}'.format(res).startswith('<ExerciseChange ')


def test_model_to_dict(db):
    ref = ExerciseChange().get_by_id(1)
    res = ref.to_dict()

    assert isinstance(res, dict)
    assert 'id' in res
    assert 'name' in res
    assert 'description' in res
    assert 'midtime' in res
    assert 'family' in res
    assert 'theme' in res


def test_model_name_readable(db):
    exercise = ExerciseChange()
    assert exercise.name_readable == ''
    exercise.name = 'invalid json'
    assert exercise.name_readable == ''
    exercise.name = '{"valid": "json"}'
    assert exercise.name_readable == ''
    exercise.name = '{"en": "exercise_name", "fr": "exercise_nom"}'
    assert exercise.name_readable == 'exercise_name'


def test_model_name_json(db):
    exercise = Exercise()
    assert exercise.name_json == {}
    exercise.name = 'invalid json'
    assert exercise.name_json == {}
    exercise.name = '{"valid": "json"}'
    assert exercise.name_json == {'valid': 'json'}


def test_model_description_json(db):
    exercise = ExerciseChange()
    assert exercise.description_json == {}
    exercise.description = 'invalid json'
    assert exercise.description_json == {}
    exercise.description = '{"valid": "json"}'
    assert exercise.description_json == {'valid': 'json'}


def test_model_get_by_id(db):
    exercise = ExerciseChange().get_by_id(1)

    # testing fields
    assert isinstance(exercise, ExerciseChange)
    assert isinstance(exercise.id, int)
    assert isinstance(exercise.parent_id, int)
    assert isinstance(exercise.author_id, int)
    assert isinstance(exercise.name, str)
    assert isinstance(exercise.description, str)
    assert isinstance(exercise.midtime, bool)
    assert isinstance(exercise.family_id, int)
    assert isinstance(exercise.theme_id, int)
    assert isinstance(exercise.ts_created, datetime)


def test_model_get_all(db):
    res = ExerciseChange().get_all()

    assert len(res) > 0
    assert all([isinstance(x, ExerciseChange) for x in res])


def test_model_save(db):
    exerciseChange = ExerciseChange().get_by_id(1)
    other_exercise = Exercise().get_by_id(2)

    exerciseChange.name = other_exercise.name
    exerciseChange.description = other_exercise.description
    exerciseChange.midtime = other_exercise.midtime
    exerciseChange.family_id = other_exercise.family_id
    exerciseChange.theme_id = other_exercise.theme_id
    exerciseChange.save()

    res = ExerciseChange().get_by_id(1)
    assert res.name == other_exercise.name
    assert res.description == other_exercise.description
    assert res.midtime == other_exercise.midtime
    assert res.family_id == other_exercise.family_id
    assert res.theme_id == other_exercise.theme_id


def test_model_apply(db):
    exerciseChange = ExerciseChange().get_by_id(1)
    refExercise = exerciseChange.parent.to_dict()
    exerciseChange.name = '{"en": "new_name", "fr": "nouveau_nom"}'
    exerciseChange.apply()
    updatedExercise = Exercise().get_by_id(refExercise.get('id'))
    assert refExercise.get('name') != updatedExercise.to_dict().get('name')
    assert refExercise.get('family') == updatedExercise.to_dict().get('family')
    assert refExercise.get('theme') == updatedExercise.to_dict().get('theme')


def test_model_delete(db):
    exerciseChange = ExerciseChange().get_by_id(1)
    exerciseChange.delete()

    exerciseChange = ExerciseChange().get_by_id(1)
    assert exerciseChange is None
