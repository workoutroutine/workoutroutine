#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

import json
from datetime import datetime
from flask import g

import pytest

from tests.conftest import HEADERS
from tests.conf_fixtures import TEST_USER
from wrapi.exercise.model import Exercise
from wrapi.routine.model import Routine, RoutineExercises
from wrapi.routine.controller import getRoutineFromCache, getCatalogFromDB, \
    genRoutine
from wrapi.utils import base64Encode


def test_model_init(db):
    res = Routine()
    assert isinstance(res, Routine)
    assert res.size == 0
    assert res.id is None
    assert len(res.uuid) > 0
    assert res.user_id is None
    assert isinstance(res.exercises, list)
    assert len(res.exercises) == 0
    assert len(res.configuration) > 0
    assert isinstance(res.ts_created, datetime)

    res = Routine(size=10)
    assert isinstance(res, Routine)
    assert res.size == 10

    res = Routine(
        id=42, size=10, routine_uuid='e8ebd5a4-236c-47e8-b25c-a30a8d868438',
        user_id=1)
    assert isinstance(res, Routine)
    assert res.size == 10
    assert res.id == 42
    assert res.uuid == 'e8ebd5a4-236c-47e8-b25c-a30a8d868438'
    assert res.user_id == 1


def test_model_repr(db):
    res = Routine()
    assert '{}'.format(res).startswith('<Routine ')


def test_model_as_list(db):
    exercises = [
        Exercise().get_by_id(1),
        Exercise().get_by_id(2),
        Exercise().get_by_id(3)
    ]
    res = Routine(size=len(exercises))
    res.exercises = exercises
    assert isinstance(res.as_list(), list)
    assert len(res.as_list()) == len(exercises)
    assert all([isinstance(x, dict) for x in res.as_list()])


def test_model_to_dict(db):
    exercises = [
        Exercise().get_by_id(1),
        Exercise().get_by_id(2),
        Exercise().get_by_id(3)
    ]
    res = Routine(size=len(exercises))
    res.exercises = exercises
    res.author = TEST_USER
    res.user_id = TEST_USER.id
    assert res.to_dict() is not None
    assert isinstance(res.to_dict(), dict)
    assert res.to_dict().get('uuid') == res.safe_id
    assert res.to_dict().get('author') == TEST_USER.to_public_dict()


def test_model_to_json(db):
    res = Routine.query.get(1)
    assert res.to_json() is not None
    assert isinstance(res.to_json(), str)


def test_model_safe_id(db):
    res = Routine.query.get(1)
    assert res.safe_id is not None
    assert isinstance(res.safe_id, str)
    assert res.safe_id == base64Encode(u=res.uuid_object)


def test_model_exercises_setter(db):
    res = Routine(size=2)
    with pytest.raises(TypeError):
        res.exercises = [42, 42]
    with pytest.raises(ValueError):
        res.exercises = [
            Exercise().get_by_id(1),
            Exercise().get_by_id(2),
            Exercise().get_by_id(3)
        ]
    res.exercises = [
        Exercise().get_by_id(1),
        Exercise().get_by_id(2)
    ]
    assert len(res.exercises) == 2


def test_model_configuration_json(db):
    res = Routine.query.get(1)
    res.configuration = "not a json"
    assert res.configuration_json == {}
    db.session.rollback()
    res.configuration = '{"foo": "bar"}'
    assert res.configuration_json == {'foo': 'bar'}
    db.session.rollback()


def test_model_save(db):
    exercises = [
        Exercise().get_by_id(1),
        Exercise().get_by_id(2),
        Exercise().get_by_id(3)
    ]
    res = Routine(size=len(exercises))
    res.exercises = exercises
    res.user_id = TEST_USER.id
    assert res.id is None
    res.save()
    assert res.id is not None
    assert Routine.query.get(res.id) is not None
    res.save()
    assert res.id is not None


def test_model_delete(db):
    exercises = [
        Exercise().get_by_id(1),
        Exercise().get_by_id(2),
        Exercise().get_by_id(3)
    ]
    res = Routine(size=len(exercises))
    res.exercises = exercises
    res.user_id = TEST_USER.id
    assert res.id is None
    res.save()
    resultId = res.id
    assert resultId is not None
    assert Routine.query.get(resultId) is not None
    res.delete()
    assert Routine.query.get(resultId) is None


def test_model_get_by_uuid(db):
    exercises = [
        Exercise().get_by_id(1),
        Exercise().get_by_id(2),
        Exercise().get_by_id(3)
    ]
    routine = Routine(size=len(exercises))
    routine.exercises = exercises
    routine.user_id = TEST_USER.id
    assert routine.id is None
    routine.save()
    routineUUID = routine.safe_id
    res = Routine().get_by_uuid(safe_id=routineUUID)
    assert res is not None
    assert isinstance(res, Routine)
    routine.delete()
    res = Routine().get_by_uuid(safe_id=routineUUID)
    assert res is None


def test_model_archive(db):
    routine = Routine.query.get(1)
    assert routine is not None
    assert routine.ts_archived is None
    routine.archive()
    assert len(RoutineExercises().get_exercises(routine_id=routine.id)) == 0
    assert routine.ts_archived is not None


def test_model_get_exercises(db):
    exercises = [
        Exercise().get_by_id(1),
        Exercise().get_by_id(2),
        Exercise().get_by_id(3)
    ]
    routine = Routine(size=len(exercises))
    routine.exercises = exercises
    routine.user_id = TEST_USER.id
    assert routine.id is None
    routine.save()

    res = RoutineExercises().get_exercises(routine_id=routine.id)
    assert res == exercises


def test_model_delete_by_routine(db):
    exercises = [
        Exercise().get_by_id(1),
        Exercise().get_by_id(2),
        Exercise().get_by_id(3)
    ]
    routine = Routine(size=len(exercises))
    routine.exercises = exercises
    routine.user_id = TEST_USER.id
    assert routine.id is None
    routine.save()

    assert len(RoutineExercises().get_exercises(routine_id=routine.id)) > 0
    RoutineExercises().delete_by_routine(routine_id=routine.id)
    assert len(RoutineExercises().get_exercises(routine_id=routine.id)) == 0


def test_controller_getRoutineFromCache(db):
    routine = Routine.query.get(1)
    res = getRoutineFromCache(id=routine.safe_id)
    assert res == {}
    assert getRoutineFromCache() == {}

    g.cache.set(routine.safe_id, routine.to_json(), expire=86400)
    res = getRoutineFromCache(id=routine.safe_id)
    assert res != {}


def test_controller_getCatalogFromDB(db):
    res = getCatalogFromDB()
    assert res is not None
    assert isinstance(res, dict)
    assert len(list(res.keys())) > 0

    res = getCatalogFromDB(family=['nofamily'], theme=['notheme'])
    assert res is not None
    assert isinstance(res, dict)
    assert len(list(res.keys())) == 0

    res = getCatalogFromDB(family='nofamily', theme='notheme')
    assert res is not None
    assert isinstance(res, dict)
    assert len(list(res.keys())) == 0

    res = getCatalogFromDB(family=['abs'], theme=['notheme'])
    assert res is not None
    assert isinstance(res, dict)
    assert len(list(res.keys())) > 0

    res = getCatalogFromDB(family=['nofamily'], theme=['fitness'])
    assert res is not None
    assert isinstance(res, dict)
    assert len(list(res.keys())) > 0

    res = getCatalogFromDB(family=['abs'], theme=['fitness'])
    assert res is not None
    assert isinstance(res, dict)
    assert len(list(res.keys())) > 0

    res = getCatalogFromDB(family=['back'], theme=['silat'])
    assert res is not None
    assert isinstance(res, dict)
    assert len(list(res.keys())) == 0


def test_controller_genRoutine(db):
    res = genRoutine()
    assert res is not None
    assert isinstance(res, Routine)
    assert res.size == 0
    assert len(res.exercises) == 0

    res = genRoutine(size=10)
    assert res is not None
    assert isinstance(res, Routine)
    assert res.size == 10
    assert len(res.exercises) == 0

    res = genRoutine(size=10, family=['abs'])
    assert res is not None
    assert isinstance(res, Routine)
    assert res.size == 10
    assert len(res.exercises) == 0

    res = genRoutine(size=10, theme=['fitness'])
    assert res is not None
    assert isinstance(res, Routine)
    assert res.size == 10
    assert len(res.exercises) == 0

    res = genRoutine(size=10, family=['abs'], theme=['fitness'])
    assert res is not None
    assert isinstance(res, Routine)
    assert res.size == 10
    assert len(res.exercises) == 10

    res = genRoutine(size=10, family=['nofamily'], theme=['notheme'])
    assert res is not None
    assert isinstance(res, Routine)
    assert res.size == 10
    assert len(res.exercises) == 0

    res = genRoutine(size=10, family=['balance'], theme=['fitness'])
    assert res is not None
    assert isinstance(res, Routine)
    assert res.size == 10
    assert len(res.exercises) == 10
    assert all([x.id == res.exercises[0].id for x in res.exercises])


def test_views_generate(client):
    call = client.post('/v1/routine/generate')
    assert call.status_code == 200
    res = json.loads(call.data)
    assert len(res.get('uuid')) > 0
    assert len(res.get('exercises')) == 0

    call = client.post(
        '/v1/routine/generate',
        data=json.dumps({
            'size': 10,
            'family': ['abs'],
            'theme': ['fitness'],
        }),
        headers=HEADERS
    )
    assert call.status_code == 200
    res = json.loads(call.data)
    assert len(res.get('uuid')) > 0
    assert len(res.get('exercises')) == 10

    call = client.post(
        '/v1/routine/generate',
        data=json.dumps({
            'size': 1000,
            'family': ['abs'],
            'theme': ['fitness'],
        }),
        headers=HEADERS
    )
    assert call.status_code == 200
    res = json.loads(call.data)
    assert len(res.get('uuid')) > 0
    assert len(res.get('exercises')) == 100
