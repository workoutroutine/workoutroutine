#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>
from auth0.v3.authentication import GetToken
from auth0.v3.management import Auth0


class AuthApi(object):
    """AuthApi is an interface to Auth0 API
    """

    def __init__(self, domain, client_id, client_secret, audience):
        self.auth0config = {
            'domain': domain,
            'client_id': client_id,
            'client_secret': client_secret,
            'audience': audience
        }
        self.get_token()
        self.get_api()

    def get_token(self):
        gtoken = GetToken(self.auth0config['domain'])
        token = gtoken.client_credentials(
            self.auth0config['client_id'],
            self.auth0config['client_secret'],
            'https://{}/api/v2/'.format(self.auth0config['domain'])
        )
        self.token = token['access_token']

    def get_api(self):
        self.auth0 = Auth0(self.auth0config['domain'], self.token)

    def get_user(self, user):
        return self.auth0.users.get(user)

    def delete_user(self, user_id):
        return self.auth0.users.delete(user_id)
