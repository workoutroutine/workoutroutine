#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

from flask import Blueprint, jsonify, current_app, request, abort
from wrapi.auth import requires_auth, AuthError
from .model import Exercise, ExerciseChange
from wrapi.family.model import Family
from wrapi.theme.model import Theme
from wrapi.user.controller import get_current_user
from wrapi.group import GROUP_ADMIN, GROUP_COACH
import json

exercise = Blueprint('exercise', __name__, url_prefix='/v1/exercise')


@exercise.route('/', methods=['GET'])
@requires_auth
def index():
    """List all the Exercise available
    """
    return jsonify([x.to_dict() for x in Exercise().get_all()])


@exercise.route('/', methods=['POST'])
@requires_auth
def create():
    """Create an Exercise from an ExerciseChange
    """
    current_user = None
    try:
        current_user = get_current_user()
    except Exception as e:
        current_app.logger.debug('Invalid user: {}'.format(e))
        raise AuthError({
            "code": "Unauthorized",
            "description": "You need to log in to access to this resource"
        }, 403)

    if (current_user.role is not None and
            current_user.role.slug in [GROUP_COACH, GROUP_ADMIN]):
        if request.json is not None:
            exercise = request.json.get('exercise', None)
            family = Family().get_by_slug(exercise.get('family'))
            theme = Theme().get_by_slug(exercise.get('theme'))

            dbexercise = Exercise(
                name=json.dumps(exercise.get('name')),
                description=json.dumps(exercise.get('description')),
                midtime=exercise.get('midtime'),
                family_id=family.id,
                theme_id=theme.id
            )
            dbexercise.save()

            exerciseChange = ExerciseChange().get_by_id(exercise.get('id'))
            exerciseChange.delete()
            return jsonify(dbexercise.to_dict())

        return ("Can't read input data", 500)

    raise AuthError({
        "code": "Unauthorized",
        "description": "You don't have access to this resource"
    }, 403)


@exercise.route('/changes', methods=['GET'])
@requires_auth
def indexChange():
    """List all the ExerciseChange available
    """
    current_user = None
    try:
        current_user = get_current_user()
    except Exception as e:
        current_app.logger.debug('Invalid user: {}'.format(e))
        raise AuthError({
            "code": "Unauthorized",
            "description": "You need to log in to access to this resource"
        }, 403)

    if (current_user.role is not None and
            current_user.role.slug in [GROUP_ADMIN]):
        return jsonify([x.to_dict() for x in ExerciseChange().get_all()])

    raise AuthError({
        "code": "Unauthorized",
        "description": "You don't have access to this resource"
    }, 403)


@exercise.route('/<int:id>', methods=['PUT'])
@requires_auth
def createChange(id):
    """Create an ExerciseChange
    """
    current_user = None
    try:
        current_user = get_current_user()
    except Exception as e:
        current_app.logger.debug('Invalid user: {}'.format(e))
        raise AuthError({
            "code": "Unauthorized",
            "description": "You need to log in to access to this resource"
        }, 403)

    if (current_user.role is not None and
            current_user.role.slug in [GROUP_COACH, GROUP_ADMIN]):
        if request.json is not None:
            exercise = request.json.get('exercise', None)
            family_id = None
            family = Family().get_by_slug(exercise.get('family'))
            if family is not None:
                family_id = family.id
            theme_id = None
            theme = Theme().get_by_slug(exercise.get('theme'))
            if theme is not None:
                theme_id = theme.id
            parent = exercise.get('id')
            if parent == 0:
                parent = None

            exerciseChange = ExerciseChange(
                parent_id=parent,
                author_id=current_user.id,
                name=json.dumps(exercise.get('name', {})),
                description=json.dumps(exercise.get('description', {})),
                midtime=exercise.get('midtime', False),
                family_id=family_id,
                theme_id=theme_id
            )
            exerciseChange.save()
            return jsonify(exerciseChange.to_dict())

        return ("Can't read input data", 500)

    raise AuthError({
        "code": "Unauthorized",
        "description": "You don't have access to this resource"
    }, 403)


@exercise.route('/changes/<int:id>', methods=['PUT'])
@requires_auth
def applyChange(id):
    """Apply a ExerciseChange on an Exercise
    """
    current_user = None
    try:
        current_user = get_current_user()
    except Exception as e:
        current_app.logger.debug('Invalid user: {}'.format(e))
        raise AuthError({
            "code": "Unauthorized",
            "description": "You need to log in to access to this resource"
        }, 403)

    if (current_user.role is not None and
            current_user.role.slug in [GROUP_ADMIN]):
        exerciseChange = ExerciseChange().get_by_id(id)
        if exerciseChange is not None:
            exercise = exerciseChange.apply()
            exerciseChange.delete()
            return jsonify(exercise.to_dict())
        abort(404)

    raise AuthError({
        "code": "Unauthorized",
        "description": "You don't have access to this resource"
    }, 403)


@exercise.route('/changes/<int:id>', methods=['DELETE'])
@requires_auth
def deleteChange(id):
    """Delete a ExerciseChange
    """
    current_user = None
    try:
        current_user = get_current_user()
    except Exception as e:
        current_app.logger.debug('Invalid user: {}'.format(e))
        raise AuthError({
            "code": "Unauthorized",
            "description": "You need to log in to access to this resource"
        }, 403)

    if (current_user.role is not None and
            current_user.role.slug in [GROUP_ADMIN]):
        exerciseChange = ExerciseChange().get_by_id(id)
        if exerciseChange is not None:
            exerciseChange.delete()
            return ('', 204)
        abort(404)

    raise AuthError({
        "code": "Unauthorized",
        "description": "You don't have access to this resource"
    }, 403)
