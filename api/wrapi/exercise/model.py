#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

from flask import current_app
import json
import random
from datetime import datetime
from wrapi import db
from wrapi.family.model import Family
from wrapi.theme.model import Theme


class Exercise(db.Model):
    """an Exercise is something to do
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)
    description = db.Column(db.Text)
    midtime = db.Column(db.Boolean, default=False)
    family_id = db.Column(db.Integer, db.ForeignKey('family.id'))
    theme_id = db.Column(db.Integer, db.ForeignKey('theme.id'))
    ts_created = db.Column(db.DateTime, default=datetime.utcnow)
    ts_updated = db.Column(db.DateTime)

    def __repr__(self):
        """String representation of the object
        """
        return '<Exercise {}>'.format(self.name_readable)

    @property
    def name_readable(self):
        return self.name_json.get('en', '')

    @property
    def name_json(self):
        n = {}
        try:
            n = json.loads(self.name)
        except Exception:
            current_app.logger.debug("unable to load name")
        return n

    @property
    def description_json(self):
        d = {}
        try:
            d = json.loads(self.description)
        except Exception:
            current_app.logger.debug("unable to load description")
        return d

    def to_dict(self):
        """Returns a dict representation of the object
        """
        family_slug = ''
        family = Family().get_by_id(id=self.family_id)
        if family is not None:
            family_slug = family.slug

        theme_slug = ''
        theme = Theme().get_by_id(id=self.theme_id)
        if theme is not None:
            theme_slug = theme.slug

        return dict(
            id=self.id,
            name=self.name_json,
            description=self.description_json,
            midtime=self.midtime,
            family=family_slug,
            theme=theme_slug
        )

    def get_by_id(self, id):
        return self.query.get(id)

    def get_all(self):
        """Returns all the Exercises
        """
        return self.query.all()

    def update(self, name, description, midtime, family_id, theme_id):
        self.name = name
        self.description = description
        self.midtime = midtime
        self.family_id = family_id
        self.theme_id = theme_id
        self.ts_updated = datetime.utcnow()
        db.session.add(self)
        db.session.commit()

    def save(self):
        """Dumps the object in the DB
        """
        db.session.add(self)
        db.session.commit()

    def getRandomEquivalent(self):
        """Pick up a random Wokrout with the same family and theme than
        original

        Returns:
            Exercise: Random Exercise with the same family and theme
        """
        exercises = self.query.\
            filter_by(theme_id=self.theme_id).\
            filter_by(family_id=self.family_id).\
            all()

        return random.choice(exercises)


class ExerciseChange(db.Model):
    """an ExerciseChange is a proposed update of an Exercise
    if applied on the parent Exercise, the ExerciseChange is removed
    """
    id = db.Column(db.Integer, primary_key=True)
    parent_id = db.Column(db.Integer, db.ForeignKey('exercise.id'))
    author_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    name = db.Column(db.Text)
    description = db.Column(db.Text)
    midtime = db.Column(db.Boolean, default=False)
    family_id = db.Column(db.Integer, db.ForeignKey('family.id'))
    theme_id = db.Column(db.Integer, db.ForeignKey('theme.id'))
    ts_created = db.Column(db.DateTime, default=datetime.utcnow)
    parent = db.relationship('Exercise', backref='changes', lazy=True)

    def __repr__(self):
        """String representation of the object
        """
        return '<ExerciseChange {}>'.format(self.name_readable)

    @property
    def name_readable(self):
        return self.name_json.get('en', '')

    @property
    def name_json(self):
        n = {}
        try:
            n = json.loads(self.name)
        except Exception:
            current_app.logger.debug("unable to load name")
        return n

    @property
    def description_json(self):
        d = {}
        try:
            d = json.loads(self.description)
        except Exception:
            current_app.logger.debug("unable to load description")
        return d

    def to_dict(self):
        """Returns a dict representation of the object
        """
        family_slug = ''
        family = Family().get_by_id(id=self.family_id)
        if family is not None:
            family_slug = family.slug

        theme_slug = ''
        theme = Theme().get_by_id(id=self.theme_id)
        if theme is not None:
            theme_slug = theme.slug

        parent = {}
        if self.parent is not None:
            parent = self.parent.to_dict()

        return dict(
            id=self.id,
            name=self.name_json,
            description=self.description_json,
            midtime=self.midtime,
            family=family_slug,
            theme=theme_slug,
            parent=parent
        )

    def save(self):
        """Dumps the object in the DB
        """
        db.session.add(self)
        db.session.commit()

    def delete(self):
        """Deletes the object from the DB
        """
        db.session.delete(self)
        db.session.commit()

    def apply(self):
        """Apply the changes to parent and delete change
        """
        exercise = self.parent
        exercise.update(
            name=self.name,
            description=self.description,
            midtime=self.midtime,
            family_id=self.family_id,
            theme_id=self.theme_id
        )
        return exercise

    def get_by_id(self, id):
        """Returns an ExerciseChange filtered on ID
        """
        return self.query.get(id)

    def get_all(self):
        """Returns all the ExerciseChanges
        """
        return self.query.all()
