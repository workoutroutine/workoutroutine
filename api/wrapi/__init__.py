#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

import os
from flask import Flask, g
from flask_mail import Mail
from flask_cors import CORS
from dotenv import load_dotenv
from .config import config
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration
from sentry_sdk.integrations.sqlalchemy import SqlalchemyIntegration
from pymemcache.client.base import Client
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from elasticsearch import Elasticsearch


load_dotenv()
db = SQLAlchemy()
migrate = Migrate()


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)

    if app.config.get('SENTRY_DSN') != "":
        sentry_sdk.init(
            dsn=app.config.get('SENTRY_DSN'),
            environment=app.config.get('SENTRY_ENV'),
            integrations=[FlaskIntegration(), SqlalchemyIntegration()]
        )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_object(config)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # Configure CORS
    CORS(app=app, supports_credentials=True)

    with app.app_context():
        # Configure database
        db.init_app(app)
        migrate.init_app(app, db, render_as_batch=True)

        # Configure cache
        @app.before_request
        def before_request():
            g.es = Elasticsearch([app.config.get('ELASTICSEARCH_HOST')])
            if not g.es.ping():
                app.logger.debug("ES Connection failed")

            g.cache = Client((app.config.get('MEMCACHED_HOST'),
                              int(app.config.get('MEMCACHED_PORT'))))

            # Configure Mail
            g.mail = Mail(app)

        # register blueprints
        from .origin.views import origin
        app.register_blueprint(origin)
        from .exercise.views import exercise
        app.register_blueprint(exercise)
        from .routine.views import routine
        app.register_blueprint(routine)
        from .family.views import family
        app.register_blueprint(family)
        from .theme.views import theme
        app.register_blueprint(theme)
        from .user.views import user
        app.register_blueprint(user)
        from .stat.views import stat
        app.register_blueprint(stat)

        return app


app = create_app()
