#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

from wrapi import db
from datetime import datetime


class Group(db.Model):
    """Group is a group of Users
    """
    id = db.Column(db.Integer, primary_key=True)
    slug = db.Column(db.String(64), index=True, unique=True)
    ts_created = db.Column(db.DateTime, default=datetime.utcnow)
    users = db.relationship('User', backref='role', lazy=True)

    def __repr__(self):
        """String representation of the object
        """
        return '<Group {}>'.format(self.slug)

    def to_dict(self):
        return dict(
            slug=self.slug
        )

    def get_by_id(self, id):
        return self.query.get(id)

    def get_by_slug(self, slug):
        return self.query.filter_by(slug=slug).first()
