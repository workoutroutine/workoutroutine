#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

import os


class config(object):
    # Flask configuration
    SECRET_KEY = os.environ.get('SECRET_KEY', default='dev')

    # Database configuration
    DATA_PATH = os.environ.get('DATA_PATH', default=os.path.join(
        os.path.dirname(__file__),
        '../data'))
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'sqlite:///' + os.path.join(DATA_PATH, 'workoutroutine.sqlite')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # Elasticsearch configuration
    ELASTICSEARCH_HOST = os.environ.get(
        'ELASTICSEARCH_HOST', default='localhost:9200')

    # Sentry configuration
    SENTRY_DSN = os.environ.get('SENTRY_DSN', default='')
    SENTRY_ENV = os.environ.get('SENTRY_ENV', default='dev')

    # Memcached configuration
    MEMCACHED_HOST = os.environ.get('MEMCACHED_HOST', default='localhost')
    MEMCACHED_PORT = os.environ.get('MEMCACHED_PORT', default=11211)

    # Flask Mail configuration
    MAIL_SERVER = os.environ.get('MAIL_SERVER')
    MAIL_PORT = os.environ.get('MAIL_PORT')
    MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS')
    MAIL_USE_SSL = os.environ.get('MAIL_USE_SSL')
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')

    # Service desk contact mail
    SERVICEDESK_EMAIL = os.environ.get(
        'SERVICEDESK_EMAIL', default='hello@workoutroutine.site')

    # Auth0 configuration
    AUTH0_DOMAIN = os.environ.get('AUTH0_DOMAIN')
    AUTH0_AUDIENCE = os.environ.get('AUTH0_AUDIENCE')
    AUTH0_CLIENT_ID = os.environ.get('AUTH0_CLIENT_ID')
    AUTH0_CLIENT_SECRET = os.environ.get('AUTH0_CLIENT_SECRET')
