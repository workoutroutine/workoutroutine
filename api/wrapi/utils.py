#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

import base64
import uuid


def base64Encode(u):
    """Encodes a UUID to a url-safe base64 string with no padding

    Args:
        u (uuid): UUID to encode.

    Returns:
        string: The encoded url-safe base64 string
    """
    if not isinstance(u, uuid.UUID):
        raise TypeError

    return base64.urlsafe_b64encode(u.bytes).decode("utf-8").strip("=")


def base64Decode(u):
    """Decodes a url-safe base64 string in a UUID

    Args:
        u (string): URL-safe base64 string with no padding.

    Returns:
        uuid: The decoded UUID
    """
    if not isinstance(u, str):
        raise TypeError

    b = base64.urlsafe_b64decode('{}=='.format(u))
    try:
        u = uuid.UUID(bytes=b)
    except ValueError:
        return None

    return u
