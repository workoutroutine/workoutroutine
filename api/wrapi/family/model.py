#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

from flask import current_app
from wrapi import db
import json
from datetime import datetime


class Family(db.Model):
    """Family is a group of Exercises, bound by nature
    """
    id = db.Column(db.Integer, primary_key=True)
    slug = db.Column(db.String(64), index=True, unique=True)
    name = db.Column(db.Text)
    logo = db.Column(db.Text)
    exercises = db.relationship('Exercise', backref='family', lazy=True)
    ts_created = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        """String representation of the object
        """
        return '<Family {}>'.format(self.slug)

    def to_dict(self):
        return dict(
            slug=self.slug,
            name=self.name_json,
            logo=self.logo
        )

    @property
    def name_json(self):
        n = {}
        try:
            n = json.loads(self.name)
        except Exception:
            current_app.logger.debug("unable to load name")
        return n

    def get_by_id(self, id):
        return self.query.get(id)

    def get_by_slug(self, slug):
        return self.query.filter_by(slug=slug).first()

    def filter_by_slug(self, slugs):
        return self.query.filter(Family.slug.in_(slugs)).all()

    def get_all(self):
        """Returns all the Family
        """
        return self.query.all()
