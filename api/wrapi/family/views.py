#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

from flask import Blueprint, jsonify
from .model import Family

family = Blueprint('family', __name__, url_prefix='/v1/family')


@family.route('/', methods=['GET'])
def index():
    """List all the family available
    """
    return jsonify([x.to_dict() for x in Family().get_all()])
