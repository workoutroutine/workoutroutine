#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

from flask import Blueprint, request, abort, current_app, g
from flask_mail import Message
from wrapi.version import __version__

origin = Blueprint('origin', __name__, url_prefix='/v1')


@origin.route('/version', methods=['GET'])
def version():
    """Return the version of the API
    """
    return __version__


@origin.route('/servicedesk', methods=['POST'])
def servicedesk():
    """Sends email to Gitlab
    """
    if request.json.get('email') is None or request.json.get('body') is None:
        abort(400)

    msg = Message(request.json.get('body'),
                  sender=request.json.get('email'),
                  recipients=[current_app.config.get('SERVICEDESK_EMAIL')])

    g.mail.send(msg)

    return ('', 204)
