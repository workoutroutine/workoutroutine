#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

from flask import current_app
from wrapi import db
import json
from datetime import datetime


class Theme(db.Model):
    """Theme is a group of Exercises, bound by thematic
    """
    id = db.Column(db.Integer, primary_key=True)
    slug = db.Column(db.String(64), index=True, unique=True)
    name = db.Column(db.Text)
    exercises = db.relationship('Exercise', backref='theme', lazy=True)
    ts_created = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        """String representation of the object
        """
        return '<Theme {}>'.format(self.slug)

    def to_dict(self):
        return dict(
            slug=self.slug,
            name=self.name_json
        )

    @property
    def name_json(self):
        n = {}
        try:
            n = json.loads(self.name)
        except Exception:
            current_app.logger.debug("unable to load name")
        return n

    def get_by_id(self, id):
        return self.query.get(id)

    def get_by_slug(self, slug):
        return self.query.filter_by(slug=slug).first()

    def filter_by_slug(self, slugs):
        return self.query.filter(Theme.slug.in_(slugs)).all()

    def get_all(self):
        """Returns all the Theme
        """
        return self.query.all()
