#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

from flask import Blueprint, jsonify
from .model import Theme

theme = Blueprint('theme', __name__, url_prefix='/v1/theme')


@theme.route('/', methods=['GET'])
def index():
    """List all the theme available
    """
    return jsonify([x.to_dict() for x in Theme().get_all()])
