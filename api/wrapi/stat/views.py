#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

from flask import Blueprint, current_app, jsonify, request
from flask.cli import with_appcontext
from wrapi.auth import requires_auth, AuthError
from wrapi.user.controller import get_current_user
from .model import StatEvent, STATEVENT_INDEX, STATEVENT_MAPPING
from elasticsearch import Elasticsearch

stat = Blueprint('stat', __name__, url_prefix='/v1/stat')


@stat.route('/event', methods=['POST'])
@requires_auth
def submit():
    """Submits a StatEvent
    """
    current_user = None
    try:
        current_user = get_current_user()
    except Exception as e:
        current_app.logger.debug('Invalid user: {}'.format(e))
        raise AuthError({
            "code": "Unauthorized",
            "description": "You need to log in to access to this resource"
        }, 403)

    se = StatEvent()

    if request.json is not None:
        if current_user.safe_id != request.json.get('user_uuid'):
            current_app.logger.error(
                'mismatch between current user and stats event data')

        se.event_type = request.json.get('event_type')
        se.event_detail = request.json.get('event_detail')
        se.user_uuid = request.json.get('user_uuid')

        se.save()

    return jsonify(se.to_dict())


@stat.cli.command('init')
@with_appcontext
def init_statevent_es():
    """Initialize the index in ES
    """
    es = Elasticsearch([current_app.config.get('ELASTICSEARCH_HOST')])
    if not es.ping():
        current_app.logger.debug("ES Connection failed")
    res = es.indices.exists(index=STATEVENT_INDEX)
    if not res:
        res = es.indices.create(index=STATEVENT_INDEX, body='')
        current_app.logger.info('Index created for {}'.format(STATEVENT_INDEX))

    res = es.indices.get_mapping(index=STATEVENT_INDEX)
    if (not res or
            not res[STATEVENT_INDEX] or
            res[STATEVENT_INDEX].get('mappings') == {}):
        res = es.indices.put_mapping(
            index=STATEVENT_INDEX, body=STATEVENT_MAPPING)
        current_app.logger.info('Mappings added for StatEvent')
