#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

from elasticsearch_dsl import Search, A, Q
from flask import g


def gatherStatsForUser(user_uuid=None):
    result = dict()

    s = Search().using(g.es).query(
        Q('term', user_uuid=user_uuid) & Q(
            'term', event_type='ROUTINE_GENERATED')
    )
    s.aggs.metric('total_exercises', A('sum', field='event_detail.size'))
    s.aggs.bucket('families', A('terms', field='event_detail.family.keyword'))
    s.aggs.bucket('themes', A('terms', field='event_detail.theme.keyword'))
    s.aggs.bucket('routines_per_day', A('date_histogram',
                                        field='ts_created',
                                        calendar_interval="1d"))
    response = s.execute()

    result['routines_families'] = {}
    for f in response.aggregations.families.buckets:
        result['routines_families'][f['key']] = f['doc_count']

    result['routines_themes'] = {}
    for f in response.aggregations.themes.buckets:
        result['routines_themes'][f['key']] = f['doc_count']

    result['routines_per_day'] = {}
    for f in response.aggregations.routines_per_day.buckets:
        result['routines_per_day'][f['key']] = f['doc_count']

    result['routines_total'] = response.hits.total.value
    result['exercises_total'] = response.aggregations.total_exercises.value

    s = Search().using(g.es).query(
        Q('term', user_uuid=user_uuid) & Q(
            'term', event_type='ROUTINE_PLAY_END')
    )
    s.aggs.metric('total_duration_exercise',
                  A('sum', field='event_detail.total_duration_exercise'))
    s.aggs.bucket('plays_per_day', A('date_histogram',
                                     field='ts_created',
                                     calendar_interval="1d"))
    response = s.execute()

    result['plays_per_day'] = {}
    for f in response.aggregations.plays_per_day.buckets:
        result['plays_per_day'][f['key']] = f['doc_count']
    result['plays_total'] = response.hits.total.value
    result['total_duration_exercise'] = response.aggregations \
                                                .total_duration_exercise \
                                                .value

    s = Search().using(g.es).query(
        Q('term', user_uuid=user_uuid) & Q(
            'term', event_type='ROUTINE_PLAY_START')
    )
    response = s.execute()

    result['plays_start_total'] = response.hits.total.value

    return result
