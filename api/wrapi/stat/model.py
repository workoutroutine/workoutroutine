#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

from flask import g
from datetime import datetime

STATEVENT_INDEX = 'wr_stat_events'
STATEVENT_MAPPING = {
    "properties": {
        "event_type": {"type": "keyword"},
        "user_uuid": {"type": "keyword"},
        "event_detail": {"type": "object"},
        "ts_created": {"type": "date"}
    }
}
STATEVENT_TYPES = [
    'ROUTINE_GENERATED',
    'ROUTINE_PLAY_START',
    'ROUTINE_PLAY_END'
]


class StatEvent(object):
    def __init__(self, event_type=None, user_uuid=None, event_detail=None):
        self.event_type = event_type
        self.user_uuid = user_uuid
        self.event_detail = event_detail
        self.ts_created = datetime.utcnow()

    def __repr__(self):
        """String representation of the object
        """
        return '<StatEvent {}>'.format(self.event_type)

    def to_dict(self):
        return dict(
            event_type=self.event_type,
            user_uuid=self.user_uuid,
            event_detail=self.event_detail,
            ts_created=self.ts_created
        )

    def save(self):
        """Saves the object in the ES
        """
        g.es.index(index=STATEVENT_INDEX, body=self.to_dict())
