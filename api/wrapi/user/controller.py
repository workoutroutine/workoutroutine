#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

from flask import current_app
from wrapi.auth import get_auth_user
from .model import User


def get_current_user():
    """Returns the current user from the User database
    It uses the auth_id from the context, if the user is authenticated

    Returns:
        [User]: User currently connected or None
    """
    current_user = None
    auth_user = get_auth_user()

    if auth_user is not None:
        current_user = User().get_by_auth_id(auth_id=auth_user.get('sub'))
        if current_user is not None:
            current_app.logger.debug(
                'Connected user: {}'.format(current_user))
        else:
            current_app.logger.debug(
                'Unable to find db user: {}'.format(auth_user.get('sub')))
    else:
        current_app.logger.debug('Unable to find auth user')

    return current_user
