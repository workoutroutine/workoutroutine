#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

from flask import current_app
from wrapi import db
from wrapi.utils import base64Encode, base64Decode
from wrapi.group.model import Group
from datetime import datetime
import uuid
import json


class User(db.Model):
    """User is, well, a User
    """
    id = db.Column(db.Integer, primary_key=True)
    uuid = db.Column(db.String(36), index=True)
    name = db.Column(db.Text)
    email = db.Column(db.Text, unique=True)
    email_verified = db.Column(db.Boolean, default=False)
    icon = db.Column(db.Text)
    preferences = db.Column(db.Text)
    auth_id = db.Column(db.Text, index=True)
    group_id = db.Column(db.Integer, db.ForeignKey('group.id'))
    ts_created = db.Column(db.DateTime, default=datetime.utcnow)
    ts_updated = db.Column(db.DateTime)
    routines = db.relationship('Routine', backref='author', lazy=True)

    def __init__(self, id=None, name=None, email=None, email_verified=False,
                 icon=None, preferences=None, auth_id=None, group_id=None):
        """Init the User object
        """
        self.id = id
        self.uuid = str(uuid.uuid4())
        self.name = name
        self.email = email
        self.email_verified = email_verified
        self.icon = icon
        self.preferences = preferences
        self.auth_id = auth_id
        self.group_id = group_id

    def __repr__(self):
        """String representation of the object
        """
        return '<User {}>'.format(self.email)

    def to_dict(self):
        if self.ts_updated is not None:
            ts_updated = self.ts_updated.timestamp()
        else:
            ts_updated = None

        role = ''
        if hasattr(self, 'role') and isinstance(self.role, Group):
            role = self.role.slug

        return dict(
            uuid=self.safe_id,
            name=self.name,
            email=self.email,
            email_verified=self.email_verified,
            icon=self.icon,
            preferences=self.preferences_json,
            auth_id=self.auth_id,
            role=role,
            ts_created=self.ts_created.timestamp(),
            ts_updated=ts_updated
        )

    def to_public_dict(self):
        return dict(
            uuid=self.safe_id,
            name=self.name,
            icon=self.icon
        )

    @property
    def safe_id(self):
        return base64Encode(u=self.uuid_object)

    @property
    def uuid_object(self):
        return uuid.UUID(self.uuid)

    @property
    def preferences_json(self):
        n = {}
        try:
            n = json.loads(self.preferences)
        except Exception:
            current_app.logger.debug("unable to load preferences")
        return n

    def get_by_auth_id(self, auth_id):
        """Returns a User filtered on auth_id
        """
        return self.query.filter_by(
            auth_id=auth_id).first()

    def get_by_uuid(self, safe_id):
        """Returns a User filtered on auth_id
        """
        user_uuid = base64Decode(u=safe_id)
        return self.query.filter_by(
            uuid=str(user_uuid)).first()

    def get_all(self):
        """Returns all the Users
        """
        return self.query.all()
