#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

from flask import Blueprint, jsonify, request, abort, current_app, g
from wrapi.auth import requires_auth, AuthError
from flask_mail import Message
from .model import User
from wrapi.group.model import Group
from .controller import get_current_user
from wrapi.group import GROUP_ADMIN
import json
from wrapi import db
from wrapi.authapi import AuthApi
from wrapi.stat.controller import gatherStatsForUser
from datetime import datetime

user = Blueprint('user', __name__, url_prefix='/v1/user')


@user.route('/', methods=['GET'])
@requires_auth
def index():
    """List all the Users available
    """
    current_user = None
    try:
        current_user = get_current_user()
    except Exception as e:
        current_app.logger.debug('Invalid user: {}'.format(e))
        raise AuthError({
            "code": "Unauthorized",
            "description": "You need to log in to access to this resource"
        }, 403)

    if (current_user.role is not None and
            current_user.role.slug in [GROUP_ADMIN]):
        return jsonify([x.to_dict() for x in User().get_all()])

    raise AuthError({
        "code": "Unauthorized",
        "description": "You don't have access to this resource"
    }, 403)


@user.route('/me', methods=['POST'])
def register():
    """Upsert authentified user as an API user
    """
    return_user = {}
    user = None
    if request.json is not None:
        user = json.loads(request.json.get('user', None))

    if user and user.get('sub') is not None:
        dbuser = User().get_by_auth_id(auth_id=user.get('sub'))
        if dbuser is None:
            dbuser = User(
                name=user.get('nickname'),
                email=user.get('email'),
                email_verified=user.get('email_verified'),
                icon=user.get('picture'),
                preferences=json.dumps(user.get('preferences')),
                auth_id=user.get('sub')
            )
            db.session.add(dbuser)
        elif dbuser.email_verified != user.get('email_verified'):
            dbuser.email_verified = user.get('email_verified')
            dbuser.ts_updated = datetime.utcnow()

        db.session.commit()

        return_user = dbuser.to_dict()

    return jsonify(return_user)


@user.route('/<string:encodedUUID>', methods=['PUT'])
@requires_auth
def update(encodedUUID):
    """Update current an API user
    """
    return_user = {}
    user = None
    if request.json is not None:
        user = request.json.get('user', None)

    if encodedUUID is not None:
        dbuser = User().get_by_uuid(safe_id=encodedUUID)
        if dbuser is None:
            abort(404)

        dbuser.name = user.get('name')
        dbuser.icon = user.get('icon')
        dbuser.preferences = json.dumps(user.get('preferences'))
        dbuser.ts_updated = datetime.utcnow()

        db.session.commit()

        return_user = dbuser.to_dict()

    return jsonify(return_user)


@user.route('/group', methods=['PUT'])
@requires_auth
def updateGroup():
    """Update the group of an API user
    """
    return_user = {}
    current_user = None
    try:
        current_user = get_current_user()
    except Exception as e:
        current_app.logger.debug('Invalid user: {}'.format(e))
        raise AuthError({
            "code": "Unauthorized",
            "description": "You need to log in to access to this resource"
        }, 403)

    if (current_user.role is not None and
            current_user.role.slug in [GROUP_ADMIN]):

        user = None
        if request.json is not None:
            user = request.json.get('user', None)
        if user.get('auth_id') is not None:
            dbuser = User().get_by_auth_id(auth_id=user.get('auth_id'))
            if dbuser is None:
                abort(404)

        group = None
        if request.json is not None:
            group = request.json.get('group', None)
        if group is not None:
            dbgroup = Group().get_by_slug(group)
            group_id = None
            if dbgroup is not None:
                group_id = dbgroup.id

            dbuser.group_id = group_id
            dbuser.ts_updated = datetime.utcnow()

            db.session.commit()

            return_user = dbuser.to_dict()

    return jsonify(return_user)


@user.route('/plan', methods=['POST'])
@requires_auth
def requestUdateUserPlan():
    """Request an update of the plan for an API user
    """
    return_status = ('Can\'t fulfill this request', 404)
    user = None
    if request.json is not None:
        user = request.json.get('user', None)

    if user.get('uuid') is not None:
        dbuser = User().get_by_uuid(user.get('uuid'))
        if dbuser is None:
            abort(404)

        msg = Message(
            'Hi, I\'d like my account to be upgraded to {}, thanks!'
            ' {} <{}>'.format(
                request.json.get('plan'), dbuser.name, dbuser.email
            ),
            sender=dbuser.email,
            recipients=[current_app.config.get('SERVICEDESK_EMAIL')]
        )

        g.mail.send(msg)
        return_status = ('', 204)

    return return_status


@user.route('/<string:encodedUUID>', methods=['DELETE'])
@requires_auth
def delete(encodedUUID):
    """Delete current API user
    """
    if encodedUUID is not None:
        dbuser = User().get_by_uuid(safe_id=encodedUUID)
        if dbuser is None:
            abort(404)

        for routine in dbuser.routines:
            routine.delete()

        auth0 = AuthApi(
            domain=current_app.config.get('AUTH0_DOMAIN'),
            client_id=current_app.config.get('AUTH0_CLIENT_ID'),
            client_secret=current_app.config.get('AUTH0_CLIENT_SECRET'),
            audience=current_app.config.get('AUTH0_AUDIENCE')
        )

        current_app.logger.debug('Deleting user {} on Auth0 ({})'.format(
            dbuser, dbuser.auth_id))
        auth0.delete_user(dbuser.auth_id)

        db.session.delete(dbuser)
        db.session.commit()

    return ('', 204)


@user.route('/<string:encodedUUID>/stats', methods=['GET'])
def stats(encodedUUID):
    dbuser = User().get_by_uuid(safe_id=encodedUUID)
    if dbuser is not None:
        return jsonify(gatherStatsForUser(user_uuid=dbuser.safe_id))
    else:
        return jsonify({})
