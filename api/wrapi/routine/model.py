#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

from flask import current_app
import uuid
import json
from datetime import datetime
from wrapi import db
from wrapi.utils import base64Encode, base64Decode
from wrapi.exercise.model import Exercise


class RoutineExercises(db.Model):
    """Many to many relationship between routines and exercises
    """
    routine_id = db.Column(db.Integer, db.ForeignKey(
        'routine.id'), primary_key=True, index=True)
    exercise_id = db.Column(
        db.Integer, db.ForeignKey('exercise.id'), primary_key=True)
    position = db.Column(db.Integer, primary_key=True)

    def get_exercises(self, routine_id):
        r = []
        exercises = self.query \
                        .filter_by(routine_id=routine_id) \
                        .order_by(RoutineExercises.position) \
                        .all()
        for exercise in exercises:
            r.append(Exercise().get_by_id(id=exercise.exercise_id))

        return r

    def delete_by_routine(self, routine_id):
        """Deletes all RoutineExercises for a given Routine

        Args:
            routine_id (int): Routine to delete RoutineExercise for
        """
        exercises = self.query \
                        .filter_by(routine_id=routine_id) \
                        .order_by(RoutineExercises.position) \
                        .all()
        for exerciseRoutine in exercises:
            db.session.delete(exerciseRoutine)
        db.session.commit()


class Routine(db.Model):
    """a Routine is a list of Exercises
    """
    id = db.Column(db.Integer, primary_key=True)
    uuid = db.Column(db.String(36), index=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    configuration = db.Column(db.Text)
    ts_created = db.Column(db.DateTime, default=datetime.utcnow)
    ts_archived = db.Column(db.DateTime)

    def __init__(self,
                 id=None, routine_uuid=None, user_id=None, size=0,
                 family=None, theme=None):
        super(Routine, self).__init__()
        if id is not None:
            self.id = id
        if routine_uuid is None:
            self.uuid = str(uuid.uuid4())
        else:
            self.uuid = routine_uuid
        if user_id is not None:
            self.user_id = user_id
        self.size = size
        self.exercises = []
        self.configuration = json.dumps({
            'size': size,
            'family': family,
            'theme': theme
        })
        self.ts_created = datetime.utcnow()

    def __repr__(self):
        return '<Routine {}>'.format(self.safe_id)

    def as_list(self):
        """Returns a list representation of the exercise array
        """
        return [x.to_dict() for x in self.exercises]

    def to_dict(self):
        """Returns a dict representation of the full object
        """
        if self.author is None:
            author = {}
        else:
            author = self.author.to_public_dict()

        return dict(
            uuid=self.safe_id,
            ts_created=self.ts_created.timestamp(),
            exercises=self.as_list(),
            configuration=self.configuration_json,
            author=author
        )

    def to_json(self):
        """Returns a JSON representation of the full object
        """
        return json.dumps(self.to_dict())

    @property
    def safe_id(self):
        return base64Encode(u=self.uuid_object)

    @property
    def exercises(self):
        if not hasattr(self, '_exercises'):
            return []
        return self._exercises

    @exercises.setter
    def exercises(self, value):
        if (not isinstance(value, list) or
                any([not isinstance(x, Exercise) for x in value])):
            raise TypeError('exercises must be a list of Exercise!')

        if len(value) > self.size:
            raise ValueError('could not add more than {} Exercises'.format(
                self.size))

        self._exercises = value

    @property
    def uuid_object(self):
        return uuid.UUID(self.uuid)

    @property
    def configuration_json(self):
        c = {}
        try:
            c = json.loads(self.configuration)
        except Exception:
            current_app.logger.debug("unable to load configuration")
        return c

    def save(self):
        """Dumps the object in the database
        """
        r = self.query.get(self.id)
        if r is not None:
            db.session.delete(r)
        db.session.add(self)
        db.session.commit()

        RoutineExercises().delete_by_routine(routine_id=self.id)

        for position, exercise in enumerate(self.exercises):
            new_exercise = RoutineExercises(
                routine_id=self.id,
                exercise_id=exercise.id,
                position=position)
            db.session.add(new_exercise)
        db.session.commit()

    def delete(self):
        """Deletes the object in the database
        """
        RoutineExercises().delete_by_routine(routine_id=self.id)

        db.session.delete(self)
        db.session.commit()

    def get_by_uuid(self, safe_id):
        """Returns a Routine filtered on UUID
        """
        routine_uuid = base64Decode(u=safe_id)
        r = self.query.filter_by(uuid=str(routine_uuid)).first()
        if r is None:
            return r

        self.__init__(
            id=r.id,
            routine_uuid=r.uuid,
            user_id=r.user_id,
            size=r.configuration_json.get('size'),
            family=r.configuration_json.get('family'),
            theme=r.configuration_json.get('theme')
        )
        self.exercises = RoutineExercises().get_exercises(routine_id=r.id)
        return self

    def archive(self):
        """Archive the routine, i.e. remove all RoutineExercises
        and keep the object itself
        """
        RoutineExercises().delete_by_routine(routine_id=self.id)

        self.ts_archived = datetime.utcnow()
        db.session.commit()
