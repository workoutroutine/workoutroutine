#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

from flask import Blueprint, jsonify, request, abort
from flask import current_app, g
from wrapi.auth import requires_auth
from .controller import genRoutine, getRoutineFromCache
from .model import Routine
from wrapi.user.controller import get_current_user
from wrapi.user.model import User
from datetime import datetime, timedelta

routine = Blueprint('routine', __name__, url_prefix='/v1/routine')
ROUTINE_EXERCICE_TTL = 14


@routine.route('/<safe_id>', methods=['GET'])
def get(safe_id):
    """Get a specific routine from the cache or from DB
    """
    r = getRoutineFromCache(id=safe_id)
    if r == {}:
        r = Routine().get_by_uuid(safe_id=safe_id)
        if r is None:
            abort(404)
        return jsonify(r.to_dict())
    return jsonify(r)


@routine.route('/generate', methods=['POST'])
def generate():
    """Generate a routine of a specific size, with the specific families
    """
    size = 0
    families = []
    theme = []
    if request.json is not None:
        size = int(request.json.get('size', 0))
        families = request.json.get('family', [])
        theme = request.json.get('theme', [])

    if size > 100:
        size = 100

    current_app.logger.debug(
        "generating routine: size({}), families({}), themes({})".format(
            size, families, theme))

    routine = genRoutine(size=size, family=families, theme=theme)

    # if user is logged, add it to the routine, and store it
    current_user = None
    try:
        current_user = get_current_user()
    except Exception as e:
        current_app.logger.debug('Invalid user: {}'.format(e))
    if current_user is not None:
        routine.user_id = current_user.id
        routine.save()
        # save the routine in the cache for 24 hours for registered user
        try:
            g.cache.set(routine.safe_id, routine.to_json(), expire=86400)
        except Exception as e:
            current_app.logger.error("Unable to save Routine: {}".format(e))

    return jsonify(routine.to_dict())


@routine.route('/recycle', methods=['PUT'])
@requires_auth
def recycle():
    """Update a Routine by changing one Exercise by another one with the same
    specs
    """
    if request.json is not None:
        routine_id = request.json.get('routine_id', '')
        exercise_id = int(request.json.get('exercise_id', 0))
        exercise_position = int(request.json.get('exercise_position', 0))

    routine = Routine().get_by_uuid(safe_id=routine_id)

    if routine is not None:
        if (routine.exercises is not None and
                len(routine.exercises) >= exercise_position):
            exercise = routine.exercises[exercise_position]
            if exercise.id != exercise_id:
                abort(
                    500,
                    description="Wrong exercise at index ("
                                "{} instead of {})".format(
                                    exercise.id, exercise_id))
            new_exercise = exercise.getRandomEquivalent()
            routine.exercises[exercise_position] = new_exercise
            routine.save()

        # save the routine in the cache for 24 hours
        try:
            g.cache.set(routine.safe_id, routine.to_json(), expire=86400)
        except Exception as e:
            current_app.logger.error("Unable to save Routine: {}".format(e))

        return jsonify(routine.to_dict())
    abort(404, description="Routine not found ({})".format(routine_id))


@routine.route('/move', methods=['PUT'])
@requires_auth
def move():
    """Update a Routine by moving one Exercise
    """
    if request.json is not None:
        routine_id = request.json.get('routine_id', '')
        exercise_id = int(request.json.get('exercise_id', 0))
        exercise_position = int(request.json.get('exercise_position', 0))
        direction = int(request.json.get('direction', 0))

    routine = Routine().get_by_uuid(safe_id=routine_id)

    if routine is not None:
        if (routine.exercises is not None and
                len(routine.exercises) >= exercise_position):
            if (exercise_position + direction < 0 or
                    exercise_position + direction > len(routine.exercises)):
                abort(500, description="Out of bound move")
            exercise = routine.exercises[exercise_position]
            if exercise.id != exercise_id:
                abort(
                    500,
                    description="Wrong exercise at index ("
                                "{} instead of {})".format(
                                    exercise.id, exercise_id))

            # swap exercise and exercise_position + direction
            exercise_parking = routine.exercises[exercise_position + direction]
            routine.exercises[exercise_position +
                              direction] = routine.exercises[exercise_position]
            routine.exercises[exercise_position] = exercise_parking
            routine.save()

        # save the routine in the cache for 24 hours
        try:
            g.cache.set(routine.safe_id, routine.to_json(), expire=86400)
        except Exception as e:
            current_app.logger.error("Unable to save Routine: {}".format(e))

        return jsonify(routine.to_dict())
    abort(404, description="Routine not found ({})".format(routine_id))


@routine.cli.command(
    name='housekeeping',
    help='Archive routines older than {} days'.format(ROUTINE_EXERCICE_TTL)
)
def housekeepRoutine():
    for user in User().get_all():
        current_app.logger.info('Check user {}'.format(user))
        for routine in user.routines:
            if (datetime.utcnow() - routine.ts_created) > timedelta(
                    days=ROUTINE_EXERCICE_TTL):
                current_app.logger.info('Archive {}'.format(routine))
                routine.archive()
