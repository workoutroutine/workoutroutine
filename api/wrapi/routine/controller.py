#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Copyright (C) 2021 Julien Andrieux <julien@mondrieux.com>

from flask import current_app, g
import copy
import random
import json
from .model import Routine
from wrapi.exercise.model import Exercise
from wrapi.family.model import Family
from wrapi.theme.model import Theme


def genRoutine(size=0, family=[], theme=[]):
    """Generates a new Routine, with random exercise
    """
    r = Routine(
        size=size,
        family=family,
        theme=theme
    )

    if len(family) == 0:
        return r
    if len(theme) == 0:
        return r

    myCatalog = getCatalogFromDB(family, theme)

    if len(list(myCatalog.keys())) == 0:
        return r
    if all(len(myCatalog[x]) == 0 for x in list(myCatalog.keys())):
        return r

    # generate a seed
    seed = random.randrange(len(list(myCatalog.keys()))) + 1
    # keep the filtered catalog as a reference
    refCatalog = copy.deepcopy(myCatalog)

    # fill the exercise list with random picks
    ex = []
    while len(ex) < size:
        # pick a family (with a seed)
        curfam = list(myCatalog.keys())[
            (len(ex)+seed) % len(list(myCatalog.keys()))]

        # when there are exercises for this family, pick one in random
        if len(myCatalog[curfam]) > 0:
            exercise = random.choice(myCatalog[curfam])
            ex.append(exercise)

            # remove exercise from list to avoid multiple pick
            myCatalog[curfam].remove(exercise)

        # when there is no exercise left in family, reset family to reference
        if len(myCatalog[curfam]) == 0:
            myCatalog[curfam] = copy.deepcopy(refCatalog[curfam])

    r.exercises = ex

    return r


def getRoutineFromCache(id=None):
    """Retreive a Routine from the cache
    """
    try:
        r = g.cache.get(id)
    except Exception:
        current_app.logger.debug("Unable to get Routine in cache")
        r = None

    if r is not None:
        return json.loads(r)

    return {}


def getCatalogFromDB(family=[], theme=[]):
    """Builds a Catalog of Exercise from the DB

    Parameters:
        family (array of strings): List of Family slug to filter on
        theme (array of strings): List of Theme slug to filter on

    Returns:
        Object: Catalog of Exercises (see note for Format)

    Note:
        The Catalog is structured as:
        {
            "<family>": [
                <Exercise>
            ]
        }

    Example:
        {
            "abs": [{
                "name": {
                    "fr": "Abdo 90°",
                    "en": "Lying Straight Leg Raise"
                },
                "description": {
                    "fr": "french desc",
                    "en": "english desc"
                },
                "family": "abs",
                "theme": "fitness",
                "midtime": false
            }, {
                "name": {
                    "fr": "Abdo pump vertical",
                    "en": "Reverse Crunch Pulse"
                },
                "description": {
                    "fr": "french desc",
                    "en": "english desc"
                },
                "family": "abs",
                "theme": "fitness",
                "midtime": false
            }],
            "arms": [{
                "name": {
                    "fr": "Dips",
                    "en": "Dips"
                },
                "description": {
                    "fr": "french desc",
                    "en": "english desc"
                },
                "family": "arms",
                "theme": "fitness",
                "midtime": false
            }]
        }
    """
    catalog = {}

    # Get the filters
    if len(family) > 0:
        myfamilies = Family().filter_by_slug(family)
    else:
        myfamilies = Family().get_all()

    if len(theme) > 0:
        mythemes = Theme().filter_by_slug(theme)
    else:
        mythemes = Theme().get_all()

    # Apply filters to get Exercises
    if len(mythemes) > 0 and len(myfamilies) > 0:
        exercises = Exercise.query.filter(
            Exercise.theme_id.in_([t.id for t in mythemes]),
            Exercise.family_id.in_([f.id for f in myfamilies]))
    elif len(mythemes) > 0 and len(myfamilies) == 0:
        exercises = Exercise.query.filter(
            Exercise.theme_id.in_([t.id for t in mythemes]))
    elif len(mythemes) == 0 and len(myfamilies) > 0:
        exercises = Exercise.query.filter(
            Exercise.family_id.in_([f.id for f in myfamilies]))
    else:
        exercises = []

    # Format the output dict
    for exercise in exercises:
        if exercise.family.slug not in catalog:
            catalog[exercise.family.slug] = []
        catalog[exercise.family.slug].append(exercise)

    return catalog
