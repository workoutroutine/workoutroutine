module.exports = {
  pluginOptions: {
    i18n: {
      locale: "fr",
      fallbackLocale: "fr",
      localeDir: "locales",
      enableInSFC: false
    }
  },
  productionSourceMap: false,
  chainWebpack: config => {
    if (process.env.NODE_ENV === "development") {
      config.devtool("eval-source-map");
    }

    const svgRule = config.module.rule("svg");
    svgRule.uses.clear();
    svgRule
      .use("babel-loader")
      .loader("babel-loader")
      .end()
      .use("vue-svg-loader")
      .loader("vue-svg-loader");
  }
};
