import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import About from "./views/About.vue";
import Pricing from "./views/Pricing.vue";
import Admin from "./views/Admin.vue";
import Profile from "./views/Profile.vue";
import Panic from "./views/Panic.vue";
import { authGuard } from "./plugins/auth/authGuard";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/about",
      name: "about",
      component: About,
      meta: {
        title: {
          en: "WorkoutRoutine - About",
          fr: "WorkoutRoutine - À Propos"
        }
      }
    },
    {
      path: "/pricing",
      name: "pricing",
      component: Pricing,
      meta: {
        title: {
          en: "WorkoutRoutine - Pricing",
          fr: "WorkoutRoutine - Offres"
        }
      }
    },
    {
      path: "/admin",
      name: "admin",
      component: Admin,
      beforeEnter: authGuard,
      meta: {
        title: {
          en: "WorkoutRoutine - Admin",
          fr: "WorkoutRoutine - Admin"
        }
      }
    },
    {
      path: "/panic",
      name: "panic",
      component: Panic,
      meta: {
        title: {
          en: "WorkoutRoutine - Oups",
          fr: "WorkoutRoutine - Panique"
        }
      }
    },
    {
      path: "/profile",
      name: "profile",
      component: Profile,
      beforeEnter: authGuard,
      meta: {
        title: {
          en: "WorkoutRoutine - Profile",
          fr: "WorkoutRoutine - Profil"
        }
      }
    },
    {
      path: "/",
      name: "home",
      component: Home,
      meta: {
        title: {
          en: "WorkoutRoutine",
          fr: "WorkoutRoutine"
        }
      }
    },
    {
      path: "/routine/:routine_id",
      name: "routine",
      component: Home,
      meta: {
        title: {
          en: "WorkoutRoutine - Routine",
          fr: "WorkoutRoutine - Routine"
        }
      },
      props: true
    },
    { path: "*", redirect: "/panic" }
  ]
});
