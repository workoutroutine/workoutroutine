import WorkoutRoutine from "@/plugins/workoutroutine-api";

export const Mutations = {
  UPDATE_ROUTINE: "updateRoutine"
};

export const Actions = {
  GENERATE_ROUTINE: "generateRoutine",
  GET_ROUTINE: "getRoutine",
  RECYCLE_EXERCISE: "recycleExercise",
  MOVE_EXERCISE: "moveExercise"
};

export default {
  state: () => ({
    exercises: [],
    safe_id: "",
    configuration: {},
    author: {}
  }),
  mutations: {
    updateRoutine(state, payload) {
      state.exercises = payload.exercises;
      state.safe_id = payload.uuid;
      state.configuration = payload.configuration;
      state.author = payload.author;
    }
  },
  actions: {
    async generateRoutine({ commit }, args) {
      const routine = await WorkoutRoutine.generateRoutine({
        size: args && args.size ? args.size : undefined,
        family: args && args.family ? args.family : [],
        theme: args && args.theme ? args.theme : []
      });
      return new Promise((resolve, reject) => {
        if (routine === undefined) {
          reject();
        }
        commit(Mutations.UPDATE_ROUTINE, routine);
        resolve();
      });
    },
    async getRoutine({ commit }, args) {
      const routine = await WorkoutRoutine.getRoutine(args.safe_id);
      commit(Mutations.UPDATE_ROUTINE, routine);
    },
    async recycleExercise({ commit }, args) {
      const routine = await WorkoutRoutine.recycleExercise({
        routine_id: args.routine_id,
        exercise_id: args.exercise_id,
        exercise_position: args.exercise_position
      });
      commit(Mutations.UPDATE_ROUTINE, routine);
    },
    async moveExercise({ commit }, args) {
      const routine = await WorkoutRoutine.moveExercise({
        routine_id: args.routine_id,
        exercise_id: args.exercise_id,
        exercise_position: args.exercise_position,
        direction: args.direction
      });
      commit(Mutations.UPDATE_ROUTINE, routine);
    }
  },
  getters: {
    exercises: state => state.exercises,
    routine_size: state => state.exercises.length,
    safe_id: state => state.safe_id,
    configuration: state => state.configuration,
    author_uuid: state => state.author?.uuid
  }
};
