import WorkoutRoutine from "@/plugins/workoutroutine-api";
import { getLocalizedField } from "@/locales/supported-locales";

export const Mutations = {
  UPDATE_THEMES: "UpdateThemes"
};

export const Actions = {
  LIST_THEMES: "listThemes"
};

export default {
  state: () => ({
    themes: []
  }),
  mutations: {
    UpdateThemes(state, payload) {
      state.themes = payload;
    }
  },
  actions: {
    async listThemes({ commit }) {
      const themes = await WorkoutRoutine.listThemes();
      commit(Mutations.UPDATE_THEMES, themes);
    }
  },
  getters: {
    themes: state => state.themes,
    getNameByTheme: state => (slug, locale) => {
      const curtheme = state.themes.find(theme => theme.slug === slug);
      if (curtheme === undefined) {
        return "";
      }
      return getLocalizedField(curtheme.name, locale);
    }
  }
};
