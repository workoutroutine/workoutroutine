import Vue from "vue";
import Vuex from "vuex";

import routine from "./routine";
import family from "./family";
import theme from "./theme";
import user from "./user";

Vue.use(Vuex);

export default new Vuex.Store({ modules: { routine, family, theme, user } });
