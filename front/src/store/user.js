import WorkoutRoutine from "@/plugins/workoutroutine-api";

export const PREFERENCES = {
  PREFERRED_LANGUAGE: "PREFERRED_LANGUAGE",
  TRACK_ACTIVITY: "TRACK_ACTIVITY",
  CONFIGURE_DEFAULT_SIZE: "CONFIGURE_DEFAULT_SIZE",
  CONFIGURE_DEFAULT_FAMILY: "CONFIGURE_DEFAULT_FAMILY",
  CONFIGURE_DEFAULT_THEME: "CONFIGURE_DEFAULT_THEME",
  PLAYER_DEFAULT_DURATION_EXERCISE: "PLAYER_DEFAULT_DURATION_EXERCISE",
  PLAYER_DEFAULT_DURATION_REST: "PLAYER_DEFAULT_DURATION_REST",
  PLAYER_DEFAULT_SOUND: "PLAYER_DEFAULT_SOUND",
  PLAYER_DEFAULT_AUTOSCROLL: "PLAYER_DEFAULT_AUTOSCROLL"
};

export const Mutations = {
  UPDATE_USER: "UpdateUser"
};

export const Actions = {
  REGISTER_USER: "registerUser",
  UPDATE_USER: "updateUser",
  DELETE_USER: "deleteUser"
};

export default {
  state: () => ({
    user: null
  }),
  mutations: {
    UpdateUser(state, payload) {
      state.user = payload;
    }
  },
  actions: {
    async registerUser({ commit }, args) {
      const user = await WorkoutRoutine.registerUser({
        user: args
      });
      commit(Mutations.UPDATE_USER, user);
    },
    async updateUser({ commit }, args) {
      const user = await WorkoutRoutine.updateUser({
        user: args
      });
      commit(Mutations.UPDATE_USER, user);
    },
    async deleteUser({ commit }, args) {
      await WorkoutRoutine.deleteUser({
        uuid: args.uuid
      });
      commit(Mutations.UPDATE_USER, null);
    }
  },
  getters: {
    me: state => state.user,
    preferences: state => state.user?.preferences || {},
    isAdmin: state =>
      state.user?.role && state.user.role.localeCompare("admin") === 0,
    isCoach: state =>
      state.user?.role && state.user.role.localeCompare("coach") === 0
  }
};
