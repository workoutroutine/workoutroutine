import WorkoutRoutine from "@/plugins/workoutroutine-api";
import { getLocalizedField } from "@/locales/supported-locales";

export const Mutations = {
  UPDATE_FAMILIES: "UpdateFamilies"
};

export const Actions = {
  LIST_FAMILIES: "listFamilies"
};

export default {
  state: () => ({
    families: []
  }),
  mutations: {
    UpdateFamilies(state, payload) {
      state.families = payload;
    }
  },
  actions: {
    async listFamilies({ commit }) {
      const families = await WorkoutRoutine.listFamilies();
      commit(Mutations.UPDATE_FAMILIES, families);
    }
  },
  getters: {
    families: state => state.families,
    getLogoByFamily: state => slug => {
      const curfam = state.families.find(family => family.slug === slug);
      return curfam ? curfam.logo : "";
    },
    getNameByFamily: state => (slug, locale) => {
      const curfam = state.families.find(family => family.slug === slug);
      if (curfam === undefined) {
        return "";
      }
      return getLocalizedField(curfam.name, locale);
    }
  }
};
