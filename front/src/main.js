import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import i18n from "./i18n";
import "./assets/sass/main.scss";
import * as Sentry from "@sentry/browser";
import { Vue as VueIntegration } from "@sentry/integrations";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faAngleDown,
  faAngleUp,
  faBell,
  faCaretDown,
  faCaretUp,
  faCheck,
  faCheckCircle,
  faCheckSquare,
  faEdit,
  faGlobe,
  faHeart,
  faIdCard,
  faEnvelope,
  faMagic,
  faPlayCircle,
  faPlus,
  faPauseCircle,
  faRecycle,
  faSearch,
  faSignOutAlt,
  faStopCircle,
  faStopwatch,
  faUser,
  faUserEdit,
  faUserTimes,
  faTimes,
  faWindowClose
} from "@fortawesome/free-solid-svg-icons";
import {
  faInstagram,
  faTwitter,
  faGitlab
} from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import VueSocialChat from "vue-social-chat";
import WorkoutRoutineAPI from "./plugins/workoutroutine-api";
import Notifications from "./plugins/notifications";
import VCalendar from "v-calendar";
import VueSmoothScroll from "vue2-smooth-scroll";

// Import the Auth0 configuration
import { domain, clientId, audience } from "../auth_config.json";
import { Auth0Plugin } from "./plugins/auth";
// Install the authentication plugin here
Vue.use(Auth0Plugin, {
  domain,
  clientId,
  audience,
  onRedirectCallback: appState => {
    router.push(
      appState && appState.targetUrl
        ? appState.targetUrl
        : window.location.pathname
    );
  }
});

// configure FontAwesome icons
library.add(
  faAngleDown,
  faAngleUp,
  faBell,
  faCaretDown,
  faCaretUp,
  faCheck,
  faCheckCircle,
  faCheckSquare,
  faEdit,
  faGitlab,
  faGlobe,
  faHeart,
  faIdCard,
  faEnvelope,
  faInstagram,
  faMagic,
  faPlayCircle,
  faPlus,
  faPauseCircle,
  faRecycle,
  faSearch,
  faSignOutAlt,
  faStopCircle,
  faStopwatch,
  faTwitter,
  faUser,
  faUserEdit,
  faUserTimes,
  faTimes,
  faWindowClose
);
Vue.component("font-awesome-icon", FontAwesomeIcon);

Vue.config.productionTip = false;

// eslint-disable-next-line no-constant-condition
if (process.env.NODE_ENV === "development") {
  try {
    Sentry.init({
      dsn: "{{.Env.SENTRY_DSN}}",
      environment: "{{.Env.SENTRY_ENV}}",
      integrations: [new VueIntegration({ Vue, attachProps: true })]
    });
  } catch (error) {
    // do nothing about it
  }
}

Vue.use(WorkoutRoutineAPI);
Vue.use(VueSocialChat);

Vue.use(Notifications);
Vue.config.errorHandler = (err, vm) => {
  vm.$notification({ title: err.toString(), color: "danger" });
  throw err;
};

Vue.use(VCalendar);
Vue.use(VueSmoothScroll, {
  updateHistory: false
});

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount("#app");

store.subscribeAction({
  error: (action, state, err) => {
    vm.$notification({ title: err.toString(), color: "danger" });
    throw err;
  }
});
