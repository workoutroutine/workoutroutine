const supportedLocales = {
  en: "English",
  fr: "Français"
};

export const defaultLocale = "en";

export function getSupportedLocales() {
  let annotatedLocales = [];

  for (const code of Object.keys(supportedLocales)) {
    annotatedLocales.push({
      code,
      name: supportedLocales[code]
    });
  }

  return annotatedLocales;
}

export function supportedLocalesInclude(locale) {
  return Object.keys(supportedLocales).includes(locale);
}

export function getLocalizedField(field, locale) {
  if (Object.keys(field).includes(locale)) {
    return field[locale];
  }
  return field[defaultLocale];
}
