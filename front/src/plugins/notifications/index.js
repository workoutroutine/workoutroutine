import NotificationContainer from "./components/Container";

export default {
  install(Vue) {
    if (Vue.prototype.$notification) {
      return;
    }

    // Notification event bus singleton
    let vm = null;

    Object.defineProperty(Vue.prototype, "$notification", {
      get() {
        //  Capture the component calling `this.$notification`
        const caller = this;

        // Caller is a component
        if (caller instanceof Vue) {
          // Get root Vue instance
          const root = caller.$root;

          // Event bus singleton has not been setup yet
          if (!vm) {
            // Create an element to mount notifications root
            const el = document.createElement("div");
            root.$el.appendChild(el);

            // Mount the notification container component and set it on the
            // event bus singleton
            vm = new Vue({
              parent: root,
              render: h => h(NotificationContainer)
            }).$mount(el);
          }
        }

        // $notification() is callable. Return a function that emits the event.
        return notification => {
          if (typeof notification === "string") {
            vm.$emit("show", { title: notification });
          } else {
            vm.$emit("show", notification);
          }
        };
      }
    });
  }
};
