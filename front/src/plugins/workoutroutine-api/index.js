import axios from "axios";
import _ from "lodash";
import { EMPTY_ROUTINE } from "@/constants";

/** @module WorkoutRoutineAPI */

// Axios resolves the promise for all HTTP status codes. Calling code must
// handle the HTTP status code and throw an error if needed.
axios.defaults.validateStatus = null;
axios.defaults.withCredentials = true;
axios.defaults.headers.post["Content-Type"] = "application/json";
axios.defaults.baseURL = "{{.Env.WORKOUTROUTINE_API}}";
if (process.env.NODE_ENV === "development") {
  axios.defaults.baseURL = "http://localhost:5000/v1";
}

/**
 * `WorkoutRoutineAPIError` encapsulates an error during a call to the WorkoutRoutine API.
 *
 * @property {object} data Response object from a failed API call. May be `null` or `undefined`.
 * @augments {Error}
 */
class WorkoutRoutineAPIError extends Error {
  /**
   * Creates an instance of WorkoutRoutineAPIError.
   *
   * @param {string} message The error message.
   * @param {object} data The response object from the failed API call, if any.
   * @memberof WorkoutRoutineAPIError
   */
  constructor(message, data) {
    super(message);
    this.data = data;
  }
}

export { WorkoutRoutineAPIError };

/**
 * `WorkoutRoutineAPI` provides functions to interface with the WorkoutRoutine API.
 */
const WorkoutRoutineAPI = {
  /**
   * Sets the authorization token for API requests. The token will be passed in
   * all requests, using the Bearer strategy in the `Authorization` header.
   *
   * @param {string} token
   */
  setAuthorizationToken(token) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
  },
  /**
   * Fetches the version of the API.
   *
   * @returns {String} The version of the API.
   */
  async getVersion() {
    const res = await axios.get("/version");
    if (res.status !== 200) {
      throw new WorkoutRoutineAPIError("Failed to retrieve version", res.data);
    }

    return res.data;
  },
  /**
   * Fetches the list of Users.
   *
   * @returns {Array} A list of Users.
   */
  async listUsers() {
    const res = await axios.get("/user/");
    if (res.status !== 200) {
      throw new WorkoutRoutineAPIError("Failed to retrieve users", res.data);
    }

    return res.data;
  },
  /**
   * Registers the current user in the API.
   *
   * @returns {Object} A User.
   */
  async registerUser({ user }) {
    const res = await axios.post(`/user/me`, {
      user: user
    });
    if (res.status !== 200) {
      throw new WorkoutRoutineAPIError(
        "Failed to register the current user",
        res.data
      );
    }

    return res.data;
  },
  /**
   * Updates the current user in the API.
   *
   * @returns {Object} A User.
   */
  async updateUser({ user }) {
    const res = await axios.put(`/user/${user.uuid}`, {
      user: user
    });
    if (res.status !== 200) {
      throw new WorkoutRoutineAPIError(
        "Failed to update the current user",
        res.data
      );
    }

    return res.data;
  },
  /**
   * Updates the group of a user in the API.
   *
   * @param {Object} A User.
   * @param {String} A Group slug.
   * @returns {Object} A User.
   */
  async updateUserGroup({ user, group }) {
    const res = await axios.put(`/user/group`, {
      user: user,
      group: group
    });
    if (res.status !== 200) {
      throw new WorkoutRoutineAPIError(
        "Failed to update the user group",
        res.data
      );
    }

    return res.data;
  },
  /**
   * Requests the upgrade the plan of the current user in the API.
   *
   * @returns {} Nothing
   */
  async updateUserPlan({ user, plan }) {
    const res = await axios.post(`/user/plan`, {
      user: user,
      plan: plan
    });
    if (res.status !== 204) {
      throw new WorkoutRoutineAPIError(
        "Failed to request the upgrade of the plan for the current user",
        res.data
      );
    }

    return res.data;
  },
  /**
   * Deletes the current user from the API.
   *
   * @returns {Object} An empty object.
   */
  async deleteUser({ uuid }) {
    const res = await axios.delete(`/user/${uuid}`);
    if (res.status !== 204) {
      throw new WorkoutRoutineAPIError(
        "Failed to delete the current user",
        res.data
      );
    }

    return {};
  },
  /**
   * Fetches user stats.
   *
   * @param {string} safeId The encoded UUID of the current user
   * @returns {object} The requested stats
   */
  async getUserStats({ uuid }) {
    const res = await axios.get(`/user/${uuid}/stats`);
    if (res.status !== 200) {
      throw new WorkoutRoutineAPIError("Failed to get User stats", res.data);
    }
    return res.data;
  },
  /**
   * Sends a StatEvent to the API.
   *
   * @returns {Object} A StatEvent.
   */
  async submitStatEvent({ user_uuid, event_type, event_detail }) {
    const res = await axios.post(`/stat/event`, {
      user_uuid: user_uuid,
      event_type: event_type,
      event_detail: event_detail
    });
    if (res.status !== 200) {
      throw new WorkoutRoutineAPIError(
        "Failed to submit a stat event",
        res.data
      );
    }

    return res.data;
  },
  /**
   * Fetches the list of families.
   *
   * @returns {Array} A list of Families.
   */
  async listFamilies() {
    const res = await axios.get("/family/");
    if (res.status !== 200) {
      throw new WorkoutRoutineAPIError("Failed to retrieve families", res.data);
    }

    return res.data;
  },
  /**
   * Fetches the list of themes.
   *
   * @returns {Array} A list of Themes.
   */
  async listThemes() {
    const res = await axios.get("/theme/");
    if (res.status !== 200) {
      throw new WorkoutRoutineAPIError("Failed to retrieve themes", res.data);
    }

    return res.data;
  },
  /**
   * Fetches the list of Exercises.
   *
   * @returns {Array} A list of Exercises.
   */
  async listExercises() {
    const res = await axios.get("/exercise/");
    if (res.status !== 200) {
      throw new WorkoutRoutineAPIError(
        "Failed to retrieve exercises",
        res.data
      );
    }

    return res.data;
  },
  /**
   * Submits a Exercise change proposal.
   *
   * @returns {Object} The ExerciseChange.
   */
  async submitExerciseChange(exercise) {
    const exerciseId = exercise.id !== null ? exercise.id : 0;
    const res = await axios.put(`/exercise/${exerciseId}`, {
      exercise: exercise
    });
    if (res.status !== 200) {
      throw new WorkoutRoutineAPIError(
        "Failed to submit exercise change",
        res.data
      );
    }

    return res.data;
  },
  /**
   * Applies a ExerciseChange proposal on the corresponding Exercise.
   *
   * @returns {Object} The updated Exercise.
   */
  async applyExerciseChange(exercise, hasParent) {
    let res = null;
    if (hasParent) {
      res = await axios.put(`/exercise/changes/${exercise.id}`);
      if (res.status !== 200) {
        throw new WorkoutRoutineAPIError(
          "Failed to apply exercise change",
          res.data
        );
      }
    } else {
      res = await axios.post(`/exercise/`, {
        exercise: exercise
      });
      if (res.status !== 200) {
        throw new WorkoutRoutineAPIError("Failed to create exercise", res.data);
      }
    }

    return res.data;
  },
  /**
   * Deletes a Exercise change proposal.
   *
   * @returns {Object} An empty object.
   */
  async discardExerciseChange(id) {
    const res = await axios.delete(`/exercise/changes/${id}`);
    if (res.status !== 204) {
      throw new WorkoutRoutineAPIError(
        "Failed to delete exercise change",
        res.data
      );
    }

    return res.data;
  },
  /**
   * Fetches the list of ExerciseChanges.
   *
   * @returns {Array} A list of ExerciseChanges.
   */
  async listExerciseChanges() {
    const res = await axios.get("/exercise/changes");
    if (res.status !== 200) {
      throw new WorkoutRoutineAPIError(
        "Failed to retrieve exercise changes",
        res.data
      );
    }

    return res.data;
  },
  /**
   * Fetches a specific Routine.
   *
   * @param {string} routine The ID of the routine
   * @returns {object} The requested Routine
   */
  async getRoutine(routine) {
    const res = await axios.get(`/routine/${routine}`);
    if (res.status !== 200) {
      throw new WorkoutRoutineAPIError("Failed to get Routine", res.data);
    }
    return _.isEmpty(res.data) ? EMPTY_ROUTINE : res.data;
  },
  /**
   * Fetches a new Routine.
   *
   * @param {int} size The size of the Routine to generate (default: 10)
   * @param {Array} family The list of Families to use in this Routine (default: "*")
   * @param {Array} theme The list of Themes to use in this Routine (default: "fitness")
   * @returns {object} The generated Routine
   */
  async generateRoutine({ size, family, theme }) {
    const res = await axios.post(`/routine/generate`, {
      size: size ? size : 10,
      family: family.length ? family : ["*"],
      theme: theme.length ? theme : ["fitness"]
    });
    if (res.status !== 200) {
      throw new WorkoutRoutineAPIError("Failed to generate Routine", res.data);
    }
    return _.isEmpty(res.data) ? EMPTY_ROUTINE : res.data;
  },
  /**
   * Change a Exercise in a Routine with another Exercise.
   *
   * @param {string} routine_id The UUID of the Routine
   * @param {int} exercise_id The ID of the Exercise to change
   * @param {int} exercise_position The position of the Exercise in the Routine
   * @returns {object} The updated Routine
   */
  async recycleExercise({ routine_id, exercise_id, exercise_position }) {
    const res = await axios.put(`/routine/recycle`, {
      routine_id: routine_id,
      exercise_id: exercise_id,
      exercise_position: exercise_position
    });
    if (res.status !== 200) {
      throw new WorkoutRoutineAPIError(
        "Failed to recycle Exercise in Routine",
        res.data
      );
    }
    return res.data;
  },
  /**
   * Move a Exercise in a Routine.
   *
   * @param {string} routine_id The UUID of the Routine
   * @param {int} exercise_id The ID of the Exercise to change
   * @param {int} exercise_position The position of the Exercise in the Routine
   * @param {int} direction The direction of the move (-1 or +1)
   * @returns {object} The updated Routine
   */
  async moveExercise({
    routine_id,
    exercise_id,
    exercise_position,
    direction
  }) {
    const res = await axios.put(`/routine/move`, {
      routine_id: routine_id,
      exercise_id: exercise_id,
      exercise_position: exercise_position,
      direction: direction
    });
    if (res.status !== 200) {
      throw new WorkoutRoutineAPIError(
        "Failed to move Exercise in Routine",
        res.data
      );
    }
    return res.data;
  },
  /**
   * Submit a ServiceDesk message.
   *
   * @param {string} email The email of the user
   * @param {string} body The body of the message
   * @returns {bool} The status of the submission
   */
  async submitServiceDesk(email, body) {
    const res = await axios.post(`/servicedesk`, {
      email: email,
      body: body
    });
    if (res.status !== 204) {
      throw new WorkoutRoutineAPIError(
        "Failed to submit Service Desk message",
        res.data
      );
    }
    return true;
  },
  /**
   * Vue API plugin loader function.
   *
   * @param {object} Vue The Vue instance.
   */
  install(Vue) {
    if (Vue.prototype.$workoutroutine) {
      throw new Error("$workoutroutine already exists on Vue prototype");
    }

    Vue.prototype.$workoutroutine = this;
  }
};

export default WorkoutRoutineAPI;
