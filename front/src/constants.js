export const EMPTY_EXERCISE = {
  id: null,
  name: {
    en: "",
    fr: ""
  },
  description: {
    en: "",
    fr: ""
  },
  midtime: false,
  family: "",
  theme: ""
};

export const EMPTY_ROUTINE = {
  exercises: [],
  uuid: "",
  configuration: {},
  author: {}
};

export const GROUPS = ["admin", "coach"];

export const REGISTER_INCENTIVE_FREQUENCY = 5;

export const EVENTS = {
  GENERATE_ROUTINE: "eventGenerateRoutine",
  START_ROUTINE: "eventStartRoutine",
  PAUSE_ROUTINE: "eventPauseRoutine",
  STOP_ROUTINE: "eventStopRoutine"
};

export const STAT_EVENTS = {
  ROUTINE_GENERATED: "ROUTINE_GENERATED",
  ROUTINE_PLAY_START: "ROUTINE_PLAY_START",
  ROUTINE_PLAY_END: "ROUTINE_PLAY_END"
};
