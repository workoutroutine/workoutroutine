# WorkoutRoutine

### Routines without the routine

## About

Are you tired of doing the same exercises over and over again? Or waste time organizing your sessions? Then WorkoutRoutine will become your ideal training partner!

Always there to back you up, our application aims to generate random circuits and thus answers the classic question we all ask at the begin of a session: "what do we do today?".

WorkoutRoutine can also take care of the clock for you: you won't have to check your watch anymore, just focus on what you're doing!

Long story short, WorkoutRoutine it's: Your Routine without the routine.

## Run

To run the application, you can either use the [docker-compose.yml](docker/docker-compose.yml) file, or run it locally in development environment. The application is made of 2 bricks: one [Flask](https://flask.palletsprojects.com/) application, the API and one [VueJS](https://vuejs.org/) application, the front.

### Docker

Edit the [docker-compose.yml](docker/docker-compose.yml) with the proper values.

Make sure the data folder are available:

```shell
$ cd docker
docker$ mkdir data elasticdata
```

You can start the docker-compose:

```shell
docker$ docker compose up -d
```

### Development

### Services

The application relies on Elasticsearch and Memcached to store data. You need to run these services before starting the API. There are plenty of ways to do so, I'll just use docker, but feel free to do otherwise.

```shell
$ docker run -p 9200:9200 -e "discovery.type=single-node"  --name wrelastic -d docker.elastic.co/elasticsearch/elasticsearch-oss:7.10.2
$ docker run -p 11211:11211 --name wrmemcache -d memcached:alpine
```

### The API

You need to have a `virtualenv` and the dependencies install before running the application.

```shell
$ cd api/
api$ make env
api$ source venv/bin/activate
(venv) api$ make dep
(venv) api$ make install
```

Create a dot-env file to configure the API (with the right values):

```shell
(venv) api$ cat > .env << _EoF_
FLASK_ENV=development
FLASK_APP=wrapi:app
SENTRY_DSN=
SENTRY_ENV=dev
MEMCACHED_HOST=localhost
MEMCACHED_PORT=11211
MAIL_SERVER=
MAIL_PORT=
MAIL_USE_TLS=True
MAIL_USERNAME=
MAIL_PASSWORD=
SERVICEDESK_EMAIL=
AUTH0_DOMAIN=
AUTH0_AUDIENCE=
AUTH0_CLIENT_ID=
AUTH0_CLIENT_SECRET=
_EoF_
```

You can now run the DB intialization and migrations:

```
(venv) api$ flask db upgrade
```

And run the ES intialization:

```
(venv) api$ flask stat init
```

And start the application:

```shell
(venv) api$ flask run
 * Serving Flask app "wrapi:app" (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: xxx-xxx-xxx
```

### The Front

You need to have a working NodeJS v14.15 installation to run the front application.

(fresh)Install the dependencies:

```shell
$ cd front
front$ npm ci
```

And start the developement server:

```shell
front$ npm run serve
[SNIP]
 DONE  Compiled successfully in 248ms

  App running at:
  - Local:   http://localhost:8080/ 
  - Network: http://w.x.y.z:8080/
```

## Thanks

WorkoutRoutine wants to thank [Culture Silat](https://www.culture-silat.fr/) (more specifically Stéphane and [Audran](https://www.culture-silat.fr/seni-gayung-fatani/guru-audran-guillou/)), [Chrales](https://twitter.com/loderunnr), [Katell](https://www.instagram.com/katwearsgloves/) and Katy M, without whom this project wouldn't exist.

## Technologies

This project is Open Source and [available](https://gitlab.com/workoutroutine/workoutroutine) on Gitlab.

The front-end is built with [VueJS](https://vuejs.org/), using CSS framework [Bulma](https://bulma.io/), and the API is based on [Flask](https://flask.palletsprojects.com/). The application is hosted at [Scaleway](https://www.scaleway.com/).
